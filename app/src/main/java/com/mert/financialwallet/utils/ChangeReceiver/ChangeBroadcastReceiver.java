package com.mert.financialwallet.utils.ChangeReceiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mert.financialwallet.utils.ChangeReceiver.Util.NetworkUtil;

public class ChangeBroadcastReceiver extends BroadcastReceiver {

    private OnChangeStateListener listener;

    private Activity activity;

    public static boolean NETWORK_STATE;

    private static int instance = -1;

    public ChangeBroadcastReceiver(Activity activity, OnChangeStateListener listener) {
        this.activity = activity;
        this.listener = listener;
        Log.i(this.getClass().getSimpleName(), "ChangeBroadcastReceiver constructor worked.");
        if (instance == -1) {
            //Internet
            if (NetworkUtil.getConnectivityStatus(activity) == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                NETWORK_STATE = true;
            } else {
                NETWORK_STATE = false;
            }

            instance = 1;
        }

        Log.i(this.getClass().getSimpleName(), "ChangeBroadcastReceiver constructor completed.");
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.d(this.getClass().getSimpleName(), "onReceive");
        int networkStatus = NetworkUtil.getConnectivityStatusString(context);

        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if (networkStatus == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                NETWORK_STATE = false;
            } else {
                NETWORK_STATE = true;
            }
            Log.i(this.getClass().getSimpleName(), "NETWORK_STATE => " + NETWORK_STATE);
        }

        this.listener.stateChanged(NETWORK_STATE);

    }
}


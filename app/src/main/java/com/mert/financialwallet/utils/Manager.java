package com.mert.financialwallet.utils;

import android.content.Context;

public abstract class Manager {

    protected Context context;

    public Manager(Context context) {
        this.context = context;
    }
}

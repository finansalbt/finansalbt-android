package com.mert.financialwallet.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class Util {
    public static class Constants {
        public static int DELAY = 0;

        public static String PREVIOUS_ACTIVITY = "perviousActivity";

        public static float dpToPx(Context context, float valueInDp) {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
        }
    }

    public static class CategoryMenu {
        public static String OTHER = "OTHER";
        public static String CATEGORY_ADD = "CATEGORY ADD";
    }

    public static class Currency {
        public static String TURK_LIRA = "₺";
        public static String TURK_LIRA_TEXT = "TRY";
        public static String DOLAR = "$";
        public static String DOLAR_TEXT = "USD";
        public static String EURO = "€";
        public static String EURO_TEXT = "EUR";
    }
}

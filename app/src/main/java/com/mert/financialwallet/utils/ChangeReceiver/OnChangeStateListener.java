package com.mert.financialwallet.utils.ChangeReceiver;

public interface OnChangeStateListener {
    void stateChanged(boolean isNetwork);
}

package com.mert.financialwallet.utils;

import static com.mert.financialwallet.utils.ApiUtil.CODE._300;
import static com.mert.financialwallet.utils.ApiUtil.CODE._301;
import static com.mert.financialwallet.utils.ApiUtil.CODE._302;
import static com.mert.financialwallet.utils.ApiUtil.CODE._303;
import static com.mert.financialwallet.utils.ApiUtil.CODE._500;
import static com.mert.financialwallet.utils.ApiUtil.CODE._501;
import static com.mert.financialwallet.utils.ApiUtil.CODE._700;
import static com.mert.financialwallet.utils.ApiUtil.CODE._900;
import static com.mert.financialwallet.utils.ApiUtil.CODE._901;
import static com.mert.financialwallet.utils.ApiUtil.CODE._902;
import static com.mert.financialwallet.utils.ApiUtil.CODE._903;

public class ApiUtil {

    public static class CODE {
        public static String BASE_ERROR = "Error";

        public static String _500 = "The category is already exist!";
        public static String _501 = "Invalid category!";
        public static String _300 = "Invalid user!";
        public static String _301 = "E-mail is already exist!";
        public static String _302 = "Username is already exist!";
        public static String _303 = "Incorrect password!";
        public static String _700 = "Database error!";
        public static String _900 = "Invalid transaction!";
        public static String _901 = "The transaction is already exist!";
        public static String _902 = "The transaction isn't exist on user!";
        public static String _903 = "Amount can't be negative";

        public static class SIGN_IN {
            public static String SUCCESS = "Error";
        }

        public static class SIGN_UP {
            public static String SUCCESS = "Error";
        }

        public static class FORGET_PASSWORD {
            public static String SUCCESS = "Error";
        }
    }

    public static String convert(String code) {
        code = "_" + code;

        switch (code) {
            case "_300":
                return _300;

            case "_301":
                return _301;

            case "_302":
                return _302;
            
            case "_303":
                return _303;
            
            case "_500":
                return _500;
            
            case "_501":
                return _501;
            
            case "_700":
                return _700;
            
            case "_900":
                return _900;
            
            case "_901":
                return _901;
            
            case "_902":
                return _902;
            
            case "_903":
                return _903;
            
            default:
                return code;
            
        }
    }

}

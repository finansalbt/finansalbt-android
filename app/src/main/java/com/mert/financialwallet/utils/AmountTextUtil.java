package com.mert.financialwallet.utils;

import android.content.Intent;
import android.util.Log;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.ByteOrder;
import java.nio.DoubleBuffer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Locale;

public class AmountTextUtil {
    private double amount;

    public AmountTextUtil() {
        amount = 0;
    }

    public AmountTextUtil(double amount) {
        Log.i(this.getClass().getSimpleName(), "amount => " + amount);

        this.amount = amount;
    }

    public String getAmountbyString() {
        Log.i(this.getClass().getSimpleName(), "getAmountbyString amount => " + amount);
        return convertToString(amount);
    }

    public double getAmountbyDouble() {
        Log.i(this.getClass().getSimpleName(), "getAmountbyDouble amount => " + amount);
        return amount;
    }

    public String add(int number) {
        this.amount = this.amount * 10;

        double numberDouble = number;

        this.amount += (numberDouble / 100);

        this.amount = new BigDecimal(this.amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
        Log.i(this.getClass().getSimpleName(), "add number => " + number + " amount => " + this.amount);
        return convertToString(amount);
    }

    public String backSpace() {
        this.amount = new BigDecimal(this.amount).setScale(2, RoundingMode.HALF_UP).doubleValue();
        if (this.amount < 0.01d) {
            this.amount = 0.00d;
            Log.i(this.getClass().getSimpleName(), "backSpace amount => " + this.amount);
            return convertToString(this.amount);
        } else {
            char[] text = convertToString(this.amount).replace(",", "").replace(".", "").toCharArray();

            ArrayList<Integer> numbers = new ArrayList<>();
            for (int i = 0; i < text.length - 1; i++) {
                try {
                    numbers.add(Integer.parseInt(String.valueOf(text[i])));
                    Log.i(this.getClass().getSimpleName(), "backSpace textadd text[" + (i + 1) + "] text  => " + text[i] + " numbers => " + numbers.get(numbers.size() - 1));
                } catch (Exception e) {
                }
            }

            String amountText = "";
            for (int i = 0; i < numbers.size(); i++) {

                if (i == (numbers.size() - 2)) {
                    amountText += String.valueOf(".");
                }
                amountText += String.valueOf(numbers.get(i));
            }
            Log.i(this.getClass().getSimpleName(), "backSpace amountText => " + amountText);

            this.amount = Double.parseDouble(amountText);
            return convertToString(this.amount);
        }
    }

    private String convertToString(double amount) {
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb, Locale.US);
        formatter.format("%,.2f", amount);

        Log.i(this.getClass().getSimpleName(), "convertToString string amount => " + sb.toString());

        return sb.toString().replace("-", "");
    }
}

package com.mert.financialwallet;

import android.app.Application;
import android.util.Log;

import com.mert.financialwallet.data.model.User;

public class App extends Application {
    //User
    public static User user;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(App.this.getClass().getSimpleName(), "onCreate");
        user = new User();
    }

    public static User getUser() {
        Log.i(App.class.getSimpleName(), "User =>" + user.toString());
        if (user == null) {
            user = new User();
        }
        return user;
    }

    public static void setUser(User user) {
        if (user == null) {
            Log.i(App.class.getClass().getSimpleName(), "User => null");
        } else {
            Log.i(App.class.getClass().getSimpleName(), "User =>" + user.toString());
        }

        App.user = user;
    }
}

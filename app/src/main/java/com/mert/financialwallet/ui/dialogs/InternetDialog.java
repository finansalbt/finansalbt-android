package com.mert.financialwallet.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mert.financialwallet.R;

public class InternetDialog extends Dialog {

    private static boolean isShowing;

    public InternetDialog(@NonNull Context context) {
        super(context);

        try {
            create();
        } catch (Exception e) {

        }
    }

    @Override
    public void create() {
        Log.i(this.getClass().getSimpleName(), "create worked");
        super.create();

        this.setContentView(R.layout.dialog_internet);
        this.setCancelable(false);

        isShowing = false;

        Log.i(this.getClass().getSimpleName(), "create completed");
    }

    @Override
    public void dismiss() {
        Log.d(this.getClass().getSimpleName(), "dissmis");
        if (isShowing()) {
            isShowing = false;
            super.dismiss();
        }
    }

    @Override
    public void show() {
        dismiss();

        Log.d(this.getClass().getSimpleName(), "show");
        if (!isShowing) {
            try {
                super.show();
            } catch (Exception e) {
            }

            isShowing = true;
        }
    }
}

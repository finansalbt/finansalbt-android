package com.mert.financialwallet.ui.auth.signin;

import com.mert.financialwallet.ui.auth.Auth;

public interface SignIn extends Auth {

    interface View extends Auth.View {
        void verificationResponseForUser(boolean response, String reponseText);
    }

    interface Presenter extends Auth.Presenter {
        void verificationRequestForUser(String userName, String password);
    }
}

package com.mert.financialwallet.ui.dialogs.categoryDialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.mert.financialwallet.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CategoryDialog extends DialogFragment {
    //UnBinder
    Unbinder unbinder;

    //Listener
    CategorDialog listener;

    //Activity
    Activity activity;

    @BindView(R.id.editText_category_dialog)
    EditText editTextCategory;

    @BindView(R.id.button_negative_category_dialog)
    Button buttonNegative;

    @BindView(R.id.button_positive_category_dialog)
    Button buttonPositive;

    public CategoryDialog setListener(CategorDialog listener) {
        this.listener = listener;
        return CategoryDialog.this;
    }

    public CategoryDialog setActivity(Activity activity) {
        this.activity = activity;
        return CategoryDialog.this;
    }

    public CategoryDialog() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_category, container);
        //ButterKnife
        unbinder = ButterKnife.bind(CategoryDialog.this, view);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        editTextCategory.setFilters(new InputFilter[]{new InputFilter.AllCaps(),new InputFilter.LengthFilter(20)});

        Log.i(this.getClass().getSimpleName(), "create completed");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.button_negative_category_dialog)
    public void Negative(View view) {
        CategoryDialog.this.dismiss();
    }

    @OnClick(R.id.button_positive_category_dialog)
    public void Positive(View view) {
        if (editTextCategory.getText().toString().trim().matches("")) {
            Log.i(CategoryDialog.this.getClass().getSimpleName(), "Fill the empty field!");
        } else {
            listener.newCategory(editTextCategory.getText().toString().toUpperCase());
            CategoryDialog.this.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

package com.mert.financialwallet.ui.base.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.mert.financialwallet.App;
import com.mert.financialwallet.ui.base.Base;
import com.mert.financialwallet.data.DataManager;
import com.mert.financialwallet.ui.dialogs.DialogManager;

public class BaseActivity extends FragmentActivity implements Base.View, Base.Activity.View {

    //Presenter
    private BaseActivityPresenter presenter;

    //Bundle
    private Bundle bundle;

    //DataManager
    public static DataManager dataManager;

    //DialogManager
    public static DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(this.getClass().getSimpleName(), "OnCreate worked.");
        //Presenter
        this.presenter = new BaseActivityPresenter(BaseActivity.this);

        //DialogManager
        this.presenter.buildDialog(this);

        //DataManager
        this.dataManager = new DataManager(BaseActivity.this);

        //Bundle
        this.bundle = new Bundle();

        Log.i(this.getClass().getSimpleName(), "OnCreate completed.");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    //Application
    public App getApp() {
        App app = (App) getApplication();
        return app;
    }

    //Arguments
    public Bundle getArguments() {
        Log.i("BUNDLE", "getArguments : " + this.bundle.toString());
        return this.bundle;
    }

    public void setArguments(Bundle bundle) {
        Log.i("BUNDLE", "setArguments : " + bundle.toString());
        this.bundle.putAll(bundle);
    }
}

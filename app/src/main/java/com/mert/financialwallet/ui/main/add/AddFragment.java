package com.mert.financialwallet.ui.main.add;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mert.financialwallet.App;
import com.mert.financialwallet.R;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.dialogs.categoryDialog.CategoryDialog;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.utils.AmountTextUtil;
import com.mert.financialwallet.utils.Util;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AddFragment extends MainFragment implements Add.View {
    //Presenter
    private AddPresenter presenter;

    //UnBinder
    Unbinder unbinder;

    //Text Amount
    public AmountTextUtil amountTextUtil;

    //View
    @BindView(R.id.relativeLayout_view_add)
    RelativeLayout relativeLayoutView;

    //PowerMenu
    private PowerMenu powerMenu;

    //TopBar
    @BindView(R.id.imageButton_back_add)
    ImageButton imageButtonBack;
    @BindView(R.id.imageButton_check_add)
    ImageButton imageButtonCheck;
    @BindView(R.id.textView_amount_add)
    TextView textViewAmount;

    /* Content */

    //Type
    @BindView(R.id.switch_type_add)
    Switch switchType;
    @BindView(R.id.textView_type_add)
    TextView textViewType;

    //Currency
    @BindView(R.id.radioGroup_currency_add)
    RadioGroup radioGroupCurrency;
    @BindView(R.id.radioButton_currency_tl_add)
    RadioButton radioButtonTl;
    @BindView(R.id.radioButton_currency_dolar_add)
    RadioButton radioButtonDolar;
    @BindView(R.id.radioButton_currency_euro_add)
    RadioButton radioButtonEuro;
    //Category
    @BindView(R.id.button_category_add)
    Button buttonCategory;

    //Comment
    @BindView(R.id.EditText_comment_add)
    EditText editTextComment;

    /* Keys */
    @BindView(R.id.relativeLayout_keys_add)
    RelativeLayout relativeLayoutKeys;
    //Numbers
    @BindView(R.id.button_zero_add)
    Button buttonZero;
    @BindView(R.id.button_one_add)
    Button buttonOne;
    @BindView(R.id.button_two_add)
    Button buttonTwo;
    @BindView(R.id.button_three_add)
    Button buttonThree;
    @BindView(R.id.button_four_add)
    Button buttonFour;
    @BindView(R.id.button_five_add)
    Button buttonFive;
    @BindView(R.id.button_six_add)
    Button buttonSix;
    @BindView(R.id.button_seven_add)
    Button buttonSeven;
    @BindView(R.id.button_eight_add)
    Button buttonEight;
    @BindView(R.id.button_nine_add)
    Button buttonNine;

    //BackSpace
    @BindView(R.id.imageButton_backSpace_add)
    ImageButton imageButtonBackSpace;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.amountTextUtil = new AmountTextUtil();

        View view = inflater.inflate(R.layout.fragment_add, container, false);

        //ButterKnife
        unbinder = ButterKnife.bind(AddFragment.this, view);

        this.presenter = new AddPresenter(AddFragment.this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        switchType.getThumbDrawable().setColorFilter(switchType.isChecked() ? mainActivity.getResources().getColor(R.color.colorAmountPositive) : mainActivity.getResources().getColor(R.color.colorAmountNegative), PorterDuff.Mode.MULTIPLY);

        this.switchType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchType.isChecked()) {
                    presenter.transaction.setType(true);
                    textViewType.setText("Income");
                } else {
                    presenter.transaction.setType(false);
                    textViewType.setText("Expense");
                }
                switchType.getThumbDrawable().setColorFilter(switchType.isChecked() ? mainActivity.getResources().getColor(R.color.colorAmountPositive) : mainActivity.getResources().getColor(R.color.colorAmountNegative), PorterDuff.Mode.MULTIPLY);
            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.onBackPressed();
            }
        });

        imageButtonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amountTextUtil.getAmountbyDouble() < 0.01d) {
                    Toast.makeText(mainActivity, "Please, entry the amount!", Toast.LENGTH_SHORT).show();
                } else {
                    presenter.transaction = new Transaction(
                            switchType.isChecked(),
                            amountTextUtil.getAmountbyDouble(),
                            getWhichChecked(),
                            App.getUser().getCategorytIdbyName(buttonCategory.getText().toString().toUpperCase()),
                            editTextComment.getText() == null ? "" : editTextComment.getText().toString()
                    );
                    presenter.check(presenter.transaction);
                }

            }
        });

        relativeLayoutView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (relativeLayoutView != null) {
                    int heightDiff = relativeLayoutView.getRootView().getHeight() - relativeLayoutView.getHeight();
                    if (heightDiff > Util.Constants.dpToPx(mainActivity, 200)) { // if more than 200 dp, it's probably a keyboard...
                        relativeLayoutKeys.setVisibility(View.GONE);
                    } else {
                        relativeLayoutKeys.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

    }

    private String getWhichChecked() {
        if (radioButtonTl.isChecked()) {
            return Util.Currency.TURK_LIRA_TEXT;
        } else if (radioButtonDolar.isChecked()) {
            return Util.Currency.DOLAR_TEXT;
        } else if (radioButtonEuro.isChecked()) {
            return Util.Currency.EURO_TEXT;
        } else {
            return Util.Currency.TURK_LIRA_TEXT;
        }
    }

    @OnClick(R.id.button_category_add)
    public void onShowCategories(View view) {
        AddFragment.this.presenter.getCategories();
    }

    @OnClick(R.id.imageButton_check_add)
    public void onCheckClick(View v) {
        if (textViewAmount.getText().toString().equals("00,00")) {
            Toast.makeText(mainActivity, "", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.button_zero_add, R.id.button_one_add, R.id.button_two_add, R.id.button_three_add, R.id.button_four_add, R.id.button_five_add, R.id.button_six_add, R.id.button_seven_add, R.id.button_eight_add, R.id.button_nine_add})
    public void onNumbersClick(View view) {

        if (amountTextUtil.getAmountbyString().length() < 14) {
            switch (view.getId()) {
                case R.id.button_zero_add:
                    this.clickNumber(0);
                    break;
                case R.id.button_one_add:
                    this.clickNumber(1);
                    break;
                case R.id.button_two_add:
                    this.clickNumber(2);
                    break;
                case R.id.button_three_add:
                    this.clickNumber(3);
                    break;
                case R.id.button_four_add:
                    this.clickNumber(4);
                    break;
                case R.id.button_five_add:
                    this.clickNumber(5);
                    break;
                case R.id.button_six_add:
                    this.clickNumber(6);
                    break;
                case R.id.button_seven_add:
                    this.clickNumber(7);
                    break;
                case R.id.button_eight_add:
                    this.clickNumber(8);
                    break;
                case R.id.button_nine_add:
                    this.clickNumber(9);
                    break;
                default:
                    break;
            }
        }
    }

    @OnClick(R.id.imageButton_backSpace_add)
    public void onBackSpace(View view) {
        this.clickBackSpace();
    }

    @Override
    public void clickNumber(int number) {
        amountTextUtil.add(number);
        this.changeAmountText();
    }

    @Override
    public void clickBackSpace() {
        this.changeAmountText();
        textViewAmount.setText(amountTextUtil.backSpace());
    }

    @Override
    public void changeAmountText() {
        textViewAmount.setText(amountTextUtil.getAmountbyString());
    }

    @Override
    public void showPopUpMenu(ArrayList<Category> categories) {
        ArrayList<PowerMenuItem> powerMenuItems = new ArrayList<>();

        for (int i = 0; i < categories.size(); i++) {
            powerMenuItems.add(new PowerMenuItem(categories.get(i).getName(), false));
        }
        powerMenuItems.add(new PowerMenuItem(Util.CategoryMenu.CATEGORY_ADD, false));

        powerMenu = new PowerMenu.Builder(AddFragment.this.getContext())
                .addItemList(powerMenuItems)
                .setAnimation(MenuAnimation.DROP_DOWN)
                .setMenuRadius(10f)
                .setMenuShadow(10f)
                .setTextColor(mainActivity.getResources().getColor(R.color.colorTextViewPrimary))
                .setMenuColor(Color.WHITE).build();

        powerMenu.setOnMenuItemClickListener(new OnMenuItemClickListener<PowerMenuItem>() {
            @Override
            public void onItemClick(int position, PowerMenuItem item) {
                Log.i(AddFragment.this.getClass().getSimpleName(), "PowerMenu onItemClick. Item =>" + item.getTitle());

                if (powerMenu.isShowing()) {
                    powerMenu.dismiss();
                }

                if (item.getTitle() == Util.CategoryMenu.CATEGORY_ADD) {

                    CategoryDialog categoryDialog = new CategoryDialog()
                            .setListener(presenter)
                            .setActivity(mainActivity);

                    FragmentManager fm = mainActivity.getSupportFragmentManager();
                    categoryDialog.show(fm, "dialog_category");

                } else {
                    buttonCategory.setText(item.getTitle().toUpperCase());
                }

            }
        });

        powerMenu.showAsDropDown(buttonCategory);
    }

    @Override
    public void initView() {

    }

    @Override
    public void stateChanged(boolean isNetwork) {
        super.stateChanged(isNetwork);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (powerMenu != null) {
            if (powerMenu.isShowing()) {
                powerMenu.dismiss();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}

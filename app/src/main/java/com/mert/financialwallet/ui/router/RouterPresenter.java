package com.mert.financialwallet.ui.router;

import android.app.Activity;

import com.mert.financialwallet.ui.base.activity.BaseActivity;
import com.mert.financialwallet.ui.base.activity.BaseActivityPresenter;

public class RouterPresenter extends BaseActivityPresenter implements Router.Presenter {
    public RouterPresenter(BaseActivity baseActivity) {
        super(baseActivity);
    }

    @Override
    public void changeActivity(Activity activity, Class to) {
        super.changeActivity(activity, to);
    }
}

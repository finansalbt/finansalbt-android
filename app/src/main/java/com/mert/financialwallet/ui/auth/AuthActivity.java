package com.mert.financialwallet.ui.auth;

import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import com.mert.financialwallet.ui.base.activity.BaseActivity;
import com.mert.financialwallet.utils.ChangeReceiver.ChangeBroadcastReceiver;

public class AuthActivity extends BaseActivity implements Auth.View {

    //Presenter
    private AuthActivityPresenter presenter;

    //Receiver
    public ChangeBroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(AuthActivity.this.getClass().getSimpleName(), "OnCreate worked.");

        //Presenter
        this.presenter = new AuthActivityPresenter(AuthActivity.this);

        Log.i(AuthActivity.this.getClass().getSimpleName(), "OnCreate completed.");
    }

    public static IntentFilter getIntentFilter(){
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        return intentFilter;
    }

}

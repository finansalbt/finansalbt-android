package com.mert.financialwallet.ui.main.details;

import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.Main;
import com.mert.financialwallet.ui.main.details.model.Header;

import java.util.ArrayList;

public interface Details extends Main.Fragment {

    interface View extends Main.Fragment.View {

    }

    interface Presenter extends Main.Fragment.Presenter {

        ArrayList<Header> getTransactionsbyHeader(ArrayList<Transaction> transactionArrayList);

        ArrayList<Category> getCategories();

        void changeCategory(String category);

        void changeAmount(String amount);
    }
}

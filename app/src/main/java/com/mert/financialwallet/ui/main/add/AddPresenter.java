package com.mert.financialwallet.ui.main.add;

import android.util.Log;
import android.widget.Toast;

import com.mert.financialwallet.App;
import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.dialogs.categoryDialog.CategorDialog;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragmentPresenter;
import com.mert.financialwallet.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class AddPresenter extends MainFragmentPresenter implements Add.Presenter, CategorDialog, Api.AddTransaction, Api.AddCategory, Api.GetCategories {

    //Transaction
    public static Transaction transaction;

    //Fragment
    private AddFragment fragment;

    public AddPresenter(MainFragment mainFragment) {
        super(mainFragment);

        transaction = new Transaction(true, 0.00d, Util.Currency.TURK_LIRA_TEXT, App.getUser().getCategorytIdbyName("OTHER"), App.getUser().getCategorytIdbyName(Util.CategoryMenu.OTHER));

        fragment = (AddFragment) mainFragment;
    }

    @Override
    public void check(Transaction transaction) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        } catch (Exception e) {
        }
        fragment.mainActivity.dataManager.addTransaction(transaction, this);
    }

    @Override
    public void getCategories() {
        fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        fragment.mainActivity.dataManager.getCategories(this);
    }

    @Override
    public void newCategory(String category) {
        Log.i(this.getClass().getSimpleName(), "newCategory Category =>" + category);
        Category temp = new Category();
        temp.setName(category);
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        } catch (Exception e) {
        }
        fragment.mainActivity.dataManager.addCategory(temp, this);
    }

    //GetCategory
    @Override
    public void onResponse(boolean isTrue, List<Category> categories) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        }catch (Exception e){
        }
        ArrayList<Category> categoryArrayList = new ArrayList<>();
        if (categories.isEmpty()) {
            Log.i(this.getClass().getSimpleName(), "onResponse empty => " + categories.isEmpty());
            Toast.makeText(fragment.mainActivity, "Categories couldn't find!", Toast.LENGTH_SHORT).show();
        } else {
            for (Category category : categories) {
                Log.i(this.getClass().getSimpleName(), "onResponse categoryId => " + category.getId());
                categoryArrayList.add(category);
            }
        }

        if (isTrue) {
            if (!categoryArrayList.isEmpty()) {
                App.getUser().setCategories(categoryArrayList);
                fragment.showPopUpMenu(categoryArrayList);
            } else {
                Toast.makeText(fragment.mainActivity, "Categories couldn't find!", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(fragment.mainActivity, "Categories couldn't find!", Toast.LENGTH_SHORT).show();
        }
    }

    //AddCategory
    @Override
    public void onResponse(boolean isTrue, Category category, String responseText) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        }catch (Exception e){
        }

        if (isTrue && category != null) {
            try {
                fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
            } catch (Exception e) {
            }
            fragment.mainActivity.dataManager.getCategories(this);
        } else {
            Toast.makeText(fragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }

    //AddTransaction
    @Override
    public void onResponse(boolean isTrue, Transaction transaction, String responseText) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        }catch (Exception e){
        }

        if (isTrue) {
            Log.i(this.getClass().getSimpleName(), "onResponse Transaction =>" + transaction.toString());
            Toast.makeText(fragment.mainActivity, "Successful!", Toast.LENGTH_SHORT).show();
            mainFragment.mainActivity.onBackPressed();
        } else {
            Log.i(this.getClass().getSimpleName(), "onResponse Transaction => null.");
            Toast.makeText(fragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }

    //ResponseText
    @Override
    public void onFailure(String responseText) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        }catch (Exception e){
        }

        if (responseText != null) {
            Toast.makeText(fragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }
}

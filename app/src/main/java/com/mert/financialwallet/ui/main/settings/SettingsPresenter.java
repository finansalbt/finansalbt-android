package com.mert.financialwallet.ui.main.settings;

import android.widget.Toast;

import com.mert.financialwallet.App;
import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.changePassword.BodyChangePassword;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragmentPresenter;

import java.util.ArrayList;

public class SettingsPresenter extends MainFragmentPresenter implements Settings.Presenter, Api.ChangePassword, Api.DeleteCategory {
    //Fragment
    private SettingsFragment fragment;

    public SettingsPresenter(MainFragment mainFragment) {
        super(mainFragment);
        fragment = (SettingsFragment) mainFragment;
    }

    @Override
    public ArrayList<Category> getCategories() {
        ArrayList<Category> arrayList = (ArrayList<Category>) App.getUser().getCategories();
        return arrayList;
    }

    @Override
    public void sendPassword(String password) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        } catch (Exception e) {
        }
        fragment.mainActivity.dataManager.changePassword(password, this);
    }

    @Override
    public void deleteCategory(String categoryId) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        } catch (Exception e) {
        }
        fragment.mainActivity.dataManager.deleteCategory(categoryId, this);
    }

    @Override
    public void onResponse(boolean isTrue, String responseText) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }
        if (isTrue) {
            fragment.showChangeLayout(false);
            Toast.makeText(mainFragment.mainActivity, "Successful!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mainFragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResponse(boolean isTrue) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }

        if (isTrue) {
            fragment.showChangeLayout(false);
            Toast.makeText(mainFragment.mainActivity, "Successful!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(String responseText) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }
        if (responseText != null) {
            Toast.makeText(mainFragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }
}

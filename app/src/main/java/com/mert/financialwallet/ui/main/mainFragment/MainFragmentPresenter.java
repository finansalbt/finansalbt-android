package com.mert.financialwallet.ui.main.mainFragment;

import com.mert.financialwallet.ui.main.Main;
import com.mert.financialwallet.ui.main.MainPresenter;
import com.mert.financialwallet.ui.main.mainActivity.MainActivity;

public class MainFragmentPresenter extends MainPresenter implements Main.Fragment.Presenter {

    protected MainFragment mainFragment;

    public MainFragmentPresenter(MainFragment mainFragment) {
        this.mainFragment = mainFragment;
    }

    @Override
    public void changeFragment(MainActivity activity, boolean pushStack, MainFragment fragment) {
        this.mainFragment.mainActivity.changeFragment((MainActivity) activity, pushStack, (MainFragment) fragment);
    }

}

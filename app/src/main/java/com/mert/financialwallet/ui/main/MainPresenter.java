package com.mert.financialwallet.ui.main;

import android.content.Context;

import com.mert.financialwallet.ui.base.BasePresenter;
import com.mert.financialwallet.ui.main.mainActivity.MainActivity;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;

public class MainPresenter extends BasePresenter implements Main.Presenter {

    public void buildDialog(Context context) {

    }

    @Override
    public void changeFragment(MainActivity activity, boolean pushStack, MainFragment mainFragment) {
        activity.changeFragment(activity, pushStack, mainFragment);
    }
}

package com.mert.financialwallet.ui.auth;

import android.content.Context;

import com.mert.financialwallet.ui.base.Base;

public interface Auth extends Base {

    interface View extends Base.View {
    }

    interface Presenter extends Base.Presenter {
    }

    interface Activity extends Base.Activity {
        interface View extends Base.Activity.View {

        }

        interface Presenter extends Base.Activity.Presenter {
        }
    }
}

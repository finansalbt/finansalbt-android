package com.mert.financialwallet.ui.auth.signup;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.db.api.Retrofit.Retrofit;
import com.mert.financialwallet.data.model.User;
import com.mert.financialwallet.ui.auth.AuthActivityPresenter;
import com.mert.financialwallet.utils.ApiUtil;
import com.mert.financialwallet.utils.ChangeReceiver.OnChangeStateListener;
import com.mert.financialwallet.utils.ChangeReceiver.Util.NetworkUtil;

public class SignUpActivityPresenter extends AuthActivityPresenter implements SignUp.Presenter, Api.SignUp,OnChangeStateListener {

    private SignUpActivity activity;

    public SignUpActivityPresenter(SignUpActivity activity) {
        super(activity);
        this.activity = activity;
        Log.d(this.getClass().getSimpleName(), "SignUpActivityPresenter created");
    }

    @Override
    public void registerRequest(User user) {
        Log.d(this.getClass().getSimpleName(), "registerRequest User =>" + user.toString());
        if (NetworkUtil.getState(activity)) {
            try {
                activity.dialogManager.getProgressDialog(activity).show();
            }catch (Exception e){

            }
            this.activity.dataManager.signUp(user, SignUpActivityPresenter.this);
        } else {
            Toast.makeText(activity, "Internetinizi açınız !", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onResponse(boolean isTrue, String responseText) {
        Log.d(this.getClass().getSimpleName(), "onResponse. isTrue => " + isTrue + " responseText =>" + responseText);
        try {
            activity.dialogManager.getProgressDialog(activity).dismiss();
        }catch (Exception e){

        }

        if (responseText != null) {
            Toast.makeText(activity, responseText, Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(activity, "Successful!", Toast.LENGTH_SHORT).show();
        }

        if (isTrue) {
            activity.onBackPressed();
        }
    }

    @Override
    public void onFailure(String responseText) {
        Log.d(this.getClass().getSimpleName(), "onFailure. responseText =>" + responseText);
        try {
            activity.dialogManager.getProgressDialog(activity).dismiss();
        }catch (Exception e){

        }

        if (responseText != null) {
            Toast.makeText(activity, responseText, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void setChangeListener(Activity activity, OnChangeStateListener listener) {
        super.setChangeListener(activity, listener);
    }

    @Override
    public void stateChanged(boolean isNetwork) {
        if (isNetwork) {
            try {
                activity.dialogManager.getInternetDialog(activity).dismiss();
            } catch (Exception e) {

            }
        } else {
            try {
                activity.dialogManager.getInternetDialog(activity).show();
            } catch (Exception e) {

            }
        }
    }
}

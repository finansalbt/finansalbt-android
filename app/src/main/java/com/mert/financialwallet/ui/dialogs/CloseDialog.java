package com.mert.financialwallet.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.mert.financialwallet.R;

public class CloseDialog extends Dialog {
    private static boolean isShowing;

    public CloseDialog(@NonNull Context context) {
        super(context);

        this.isShowing = false;

        create();
    }

    @Override
    public void create() {
        super.create();
        Log.i(this.getClass().getSimpleName(), "create worked");

        this.setContentView(R.layout.dialog_close);

        this.setCancelable(false);

        isShowing = false;

        final Button button_positive = (Button) this.findViewById(R.id.button_posiitive_close);
        final Button button_negative = (Button) this.findViewById(R.id.button_negative_close);

        button_positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });

        button_negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseDialog.this.dismiss();
            }
        });

        Log.i(this.getClass().getSimpleName(), "create completed");
    }

    @Override
    public void dismiss() {
        Log.d(this.getClass().getSimpleName(), "dissmis");
        if (isShowing()) {
            isShowing = false;
            super.dismiss();
        }
    }

    @Override
    public void show() {
        dismiss();

        Log.d(this.getClass().getSimpleName(), "show");
        if (!isShowing) {
            try {
                super.show();
            } catch (Exception e) {
            }

            isShowing = true;
        }
    }
}

package com.mert.financialwallet.ui.main.mainActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.anychart.core.stock.indicators.MACD;
import com.mert.financialwallet.App;
import com.mert.financialwallet.R;
import com.mert.financialwallet.ui.base.activity.BaseActivity;
import com.mert.financialwallet.ui.main.Main;
import com.mert.financialwallet.ui.main.home.HomeFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.ui.router.RouterActivity;
import com.mert.financialwallet.utils.ChangeReceiver.ChangeBroadcastReceiver;
import com.mert.financialwallet.utils.ChangeReceiver.OnChangeStateListener;
import com.mert.financialwallet.utils.ChangeReceiver.Util.NetworkUtil;
import com.mert.financialwallet.utils.Util;

public class MainActivity extends BaseActivity implements Main.Activity.View, OnChangeStateListener {

    //Presenter
    public MainActivityPresenter presenter;

    //Receiver
    public ChangeBroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(this.getClass().getSimpleName(), "OnCreate worked.");
        setContentView(R.layout.activity_main);

        //Presenter
        this.presenter = new MainActivityPresenter(MainActivity.this);

        this.presenter.buildDialog(MainActivity.this);

        this.receiver = new ChangeBroadcastReceiver(MainActivity.this, presenter);

        Log.i(this.getClass().getSimpleName(), "OnCreate completed.");
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(receiver, intentFilter);

        if (NetworkUtil.getState(MainActivity.this)) {
            try {
                dialogManager.getInternetDialog(MainActivity.this).dismiss();

                initApp();

            } catch (Exception e) {

            }

        } else {
            try {
                dialogManager.getInternetDialog(MainActivity.this).show();
            } catch (Exception e) {

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    public void initApp() {
        Bundle bundle = getIntent().getExtras();
        if (bundle.getString(Util.Constants.PREVIOUS_ACTIVITY) != null) {
            if (bundle.getString(Util.Constants.PREVIOUS_ACTIVITY).equals(RouterActivity.class.getSimpleName())) {
                Log.i(this.getClass().getSimpleName(), "came from RouterActivity. initUser");
                if (!NetworkUtil.getState(this)) {
                    try {
                        dialogManager.getInternetDialog(MainActivity.this).show();
                    }catch (Exception e){

                    }
                } else {
                    try {
                        dialogManager.getInternetDialog(MainActivity.this).dismiss();
                        dialogManager.getProgressDialog(MainActivity.this).show();
                    } catch (Exception e) {
                    }
                    dataManager.initUser(presenter);
                }
            } else {
                if (!NetworkUtil.getState(this)) {
                    try {
                        dialogManager.getInternetDialog(MainActivity.this).show();
                    }catch (Exception e){

                    }
                } else {
                    try {
                        dialogManager.getInternetDialog(MainActivity.this).dismiss();
                    }catch (Exception e){

                    }
                }
                this.changeFragment(MainActivity.this, false, new HomeFragment());
                Log.i(this.getClass().getSimpleName(), "came from " + bundle.getString(Util.Constants.PREVIOUS_ACTIVITY));
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            /*Hangi Fragment'te olduğunu bulmak için.


             */
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frameLayout_main);

            if (currentFragment instanceof HomeFragment) {
                Log.i(this.getClass().getSimpleName(), "STACK BACK PRESS => " + getSupportFragmentManager().findFragmentById(R.id.frameLayout_main).toString());
                dialogManager.getCloseDialog().show();
            } else {
                Log.i(this.getClass().getSimpleName(), "STACK POP => " + getSupportFragmentManager().findFragmentById(R.id.frameLayout_main).toString());
                super.onBackPressed();
            }

            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(this);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            Log.i(this.getClass().getSimpleName(), "STACK POP => " + getSupportFragmentManager().findFragmentById(R.id.frameLayout_main).toString());
        } else {
            dialogManager.getCloseDialog().show();
        }
    }

    public void changeFragment(final MainActivity mainActivity, final boolean pushStack, final MainFragment fragment) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                /*Animasyonlu geçiş
                getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.to_right_animation, R.anim.from_left_animation, R.anim.from_right_animation, R.anim.to_left_animation).replace(R.id.frameLayout_baseActivity, fragment).addToBackStack(fragment.toString()).commit();
                */
                if (pushStack) {
                    mainActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out).replace(R.id.frameLayout_main, fragment).addToBackStack(fragment.toString()).commit();
                    Log.i(this.getClass().getSimpleName(), "Fragment Change => " + fragment.toString() + "\nSTACK PUSH => " + fragment.toString());
                } else {
                    try {
                        mainActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, R.anim.fade_out).replace(R.id.frameLayout_main, fragment).commit();
                    } catch (Exception e) {
                        changeFragment(mainActivity, pushStack, fragment);
                    }
                    Log.i(this.getClass().getSimpleName(), "Fragment init => " + fragment.toString());
                }
            }
        }, Util.Constants.DELAY);
    }

    @Override
    public void stateChanged(boolean isNetwork) {
        if (isNetwork) {
            dialogManager.getInternetDialog(MainActivity.this).show();
        } else {
            dialogManager.getInternetDialog(MainActivity.this).dismiss();
        }
    }
}

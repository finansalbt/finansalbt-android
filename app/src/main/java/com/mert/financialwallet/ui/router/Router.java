package com.mert.financialwallet.ui.router;

import com.mert.financialwallet.ui.base.Base;

public interface Router extends Base.Activity {

    interface View extends Base.Activity.View {

    }

    interface Presenter extends Base.Activity.Presenter {
    }
}

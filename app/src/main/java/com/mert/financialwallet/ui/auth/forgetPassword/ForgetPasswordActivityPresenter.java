package com.mert.financialwallet.ui.auth.forgetPassword;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.ui.auth.AuthActivityPresenter;
import com.mert.financialwallet.utils.ChangeReceiver.OnChangeStateListener;

public class ForgetPasswordActivityPresenter extends AuthActivityPresenter implements ForgetPassword.Presenter, Api.ForgetPassword ,OnChangeStateListener {

    //Fragment
    private ForgetPasswordActivity forgetPasswordActivity;

    public ForgetPasswordActivityPresenter(ForgetPasswordActivity forgetPasswordActivity) {
        super(forgetPasswordActivity);
        this.forgetPasswordActivity = forgetPasswordActivity;
        Log.d(this.getClass().getSimpleName(), "SignInFragmentPresenter created");
    }

    @Override
    public void verificationRequestForEMail(String eMail) {
        Log.d(this.getClass().getSimpleName(), "verificationRequestForEMail eMail =>" + eMail);
        // DataManager ile doğrulama yaptıktan sonra aşağıda ki metot ile gönder. //
        this.forgetPasswordActivity.verificationResponseForEMail(true);
    }

    @Override
    public void sendMail(String eMail) {
        Log.d(this.getClass().getSimpleName(), "sendMail eMail =>" + eMail);
        try {
            forgetPasswordActivity.dialogManager.getProgressDialog(forgetPasswordActivity).show();
        }catch (Exception e){
        }
        forgetPasswordActivity.dataManager.forgetPassword(eMail, this);
    }

    @Override
    public void onResponse(boolean isTrue, String responseText) {
        try {
            forgetPasswordActivity.dialogManager.getProgressDialog(forgetPasswordActivity).dismiss();
        }catch (Exception e){
        }
        if (isTrue) {
            Toast.makeText(forgetPasswordActivity, "IF YOU DON'T GET E MAIL. SEND AGAIN", Toast.LENGTH_SHORT).show();
            forgetPasswordActivity.onBackPressed();
        } else {
            Toast.makeText(forgetPasswordActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(String responseText) {
        try {
            forgetPasswordActivity.dialogManager.getProgressDialog(forgetPasswordActivity).dismiss();
        }catch (Exception e){
        }

        if (responseText != null) {
            Toast.makeText(forgetPasswordActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void setChangeListener(Activity activity, OnChangeStateListener listener) {
        super.setChangeListener(activity, listener);
    }

    @Override
    public void stateChanged(boolean isNetwork) {
        if (isNetwork) {
            try {
                forgetPasswordActivity.dialogManager.getInternetDialog(forgetPasswordActivity).dismiss();
            } catch (Exception e) {

            }
        } else {
            try {
                forgetPasswordActivity.dialogManager.getInternetDialog(forgetPasswordActivity).show();
            } catch (Exception e) {

            }
        }
    }
}

package com.mert.financialwallet.ui.main.details;

import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import com.mert.financialwallet.App;
import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.details.model.Header;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragmentPresenter;
import com.mert.financialwallet.utils.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DetailsPresenter extends MainFragmentPresenter implements Details.Presenter, Api.GetTransactions {
    //Fragment
    private DetailsFragment fragment;

    public static String CATEGORY;
    public static String AMOUNT;

    public static ArrayList<Transaction> transactions;

    public DetailsPresenter(MainFragment mainFragment) {
        super(mainFragment);
        Log.i(this.getClass().getSimpleName(), "DetailsPresenter created");
        fragment = (DetailsFragment) mainFragment;

        CATEGORY = Util.CategoryMenu.OTHER;
        AMOUNT = Util.Currency.TURK_LIRA_TEXT;

        transactions = new ArrayList<>();
    }

    @Override
    public ArrayList<Header> getTransactionsbyHeader(ArrayList<Transaction> transactionArrayList) {
        ArrayList<Header> headers = new ArrayList<>();

        ArrayList<Transaction> temp = transactionArrayList;

        ArrayList<Transaction> transactions = new ArrayList<>();

        for (int i = 0; i < temp.size(); i++) {
            Log.i(this.getClass().getSimpleName(), "getTransactionsbyHeader transaction => " + temp.get(i).toString());
            if (temp.get(i).getCurrencyUnit().toUpperCase().equals(AMOUNT)) {
                transactions.add(temp.get(i));
            }
        }

        for (int i = 0; i < transactions.size(); i++) {
            Transaction transaction = transactions.get(i);

            String year = DateFormat.format("yyyy", new Date(transaction.getCreatedAt())).toString();
            String month = DateFormat.format("MMMM", new Date(transaction.getCreatedAt())).toString();
            String headerName = month + " " + year;

            Log.i(this.getClass().getSimpleName(), "getTransactionsbyHeader header => " + headerName + " transaction => " + transaction.toString());

            if (headers.size() == 0) {
                Header header = new Header();
                header.setHeaderName(headerName);
                header.addTrasaction(transaction);
                headers.add(header);
            } else {
                int index = headers.size() - 1;
                if (headers.get(index).getHeaderName().equals(headerName)) {
                    headers.get(index).addTrasaction(transaction);
                } else {
                    Header header = new Header();
                    header.setHeaderName(headerName);
                    header.addTrasaction(transaction);
                    headers.add(header);
                }
            }
        }
        Log.i(this.getClass().getSimpleName(), "EXPANDABLE_LIST Size :" + String.valueOf(transactions.size()) + " Count : " + headers.size());
        return headers;
    }

    public ArrayList<Transaction> getTransactionsbyAmount(ArrayList<Transaction> transactions) {
        ArrayList<Transaction> transactionArrayList = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getCurrencyUnit().toUpperCase().equals(AMOUNT.toUpperCase())) {
                Log.i(this.getClass().getSimpleName(), "getTransactionsbyAmount transaction => " + transaction.toString());
                transactionArrayList.add(transaction);
            }
        }
        return transactionArrayList;
    }

    @Override
    public ArrayList<Category> getCategories() {
        ArrayList<Category> arrayList = (ArrayList<Category>) App.getUser().getCategories();
        return arrayList;
    }

    @Override
    public void changeCategory(String category) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        } catch (Exception e) {
        }

        Log.i(this.getClass().getSimpleName(), "changeCategory Category => " + category.toUpperCase());

        this.CATEGORY = category.toUpperCase();

        fragment.mainActivity.dataManager.getTransactions(App.getUser().getCategorytIdbyName(this.CATEGORY), this);
    }

    @Override
    public void changeAmount(String amount) {
        this.AMOUNT = amount;

        Log.i(this.getClass().getSimpleName(), "changeAmount Amount => " + amount.toUpperCase());
        fragment.initView(getTransactionsbyHeader(getTransactionsbyAmount(transactions)));
    }

    @Override
    public void onResponse(boolean isTrue, List<Transaction> transactions) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }

        if (isTrue) {
            if (transactions != null) {
                this.transactions = (ArrayList<Transaction>) transactions;
                fragment.initView(getTransactionsbyHeader(this.transactions));
            } else {
                Toast.makeText(mainFragment.mainActivity, "Transaction can't find!", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(mainFragment.mainActivity, "No Transaction!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onFailure(String responseText) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }

        Toast.makeText(mainFragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();

    }
}

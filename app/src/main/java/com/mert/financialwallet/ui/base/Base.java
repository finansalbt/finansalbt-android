package com.mert.financialwallet.ui.base;

import android.content.Context;

public interface Base {

    interface View {

    }

    interface Presenter {
    }

    interface Activity {
        interface View extends Base.View {

        }

        interface Presenter extends Base.Presenter {
            void changeActivity(android.app.Activity activity, Class to);
        }
    }
}

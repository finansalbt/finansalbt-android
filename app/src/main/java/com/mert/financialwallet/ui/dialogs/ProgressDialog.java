package com.mert.financialwallet.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mert.financialwallet.R;

public class ProgressDialog extends Dialog {

    private static boolean isShowing;

    public ProgressDialog(@NonNull Context context) {
        super(context);

        try {
            create();
        } catch (Exception e) {

        }
    }

    @Override
    public void create() {
        Log.d(this.getClass().getSimpleName(), "onCreate worked");
        super.create();

        this.setContentView(R.layout.dialog_progress);
        this.setCancelable(false);

        isShowing = false;

        Log.d(this.getClass().getSimpleName(), "onCreate completed");
    }

    @Override
    public void dismiss() {
        Log.d(this.getClass().getSimpleName(), "dissmis");
        if (isShowing()) {
            isShowing = false;
            super.dismiss();
        }
    }

    @Override
    public void show() {
        dismiss();

        Log.d(this.getClass().getSimpleName(), "show");
        if (!isShowing) {
            try {
                super.show();
            } catch (Exception e) {
            }

            isShowing = true;
        }
    }
}
package com.mert.financialwallet.ui.auth.forgetPassword;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mert.financialwallet.R;
import com.mert.financialwallet.ui.auth.AuthActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ForgetPasswordActivity extends AuthActivity implements ForgetPassword.View {

    //Presenter
    private ForgetPasswordActivityPresenter presenter;

    //UnBinder
    Unbinder unbinder;

    //Button
    @BindView(R.id.button_forget_password)
    Button buttonSend;
    //EditText
    @BindView(R.id.editText_email_forget_password)
    EditText editTextEMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(this.getClass().getSimpleName(), "onCreateView worked");
        setContentView(R.layout.activity_forget);
        unbinder = ButterKnife.bind(ForgetPasswordActivity.this);

        presenter = new ForgetPasswordActivityPresenter(ForgetPasswordActivity.this);
        presenter.setChangeListener(this, presenter);

        Log.d(this.getClass().getSimpleName(), "onCreateView completed");
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            registerReceiver(receiver, getIntentFilter());
        }catch (Exception e){

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(receiver);

        }catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (editTextEMail.getText() != null) {
            editTextEMail = findViewById(R.id.editText_email_forget_password);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.button_forget_password)
    void OnClick() {
        if (!isFullFields()) {
            if (editTextEMail.getText().toString().equals("")) {
                Log.d(this.getClass().getSimpleName(), "field of editTextEMail was empty");
                checkEMail(false);
            }
            Toast.makeText(ForgetPasswordActivity.this, "Fill the empty fields!", Toast.LENGTH_SHORT).show();
        } else if (!isCorrectEmail()) {
            checkEMail(false);
            Toast.makeText(ForgetPasswordActivity.this, "Input an e mail!!", Toast.LENGTH_SHORT).show();
        } else {//All true.
            Log.i(ForgetPasswordActivity.this.getClass().getSimpleName(), "//All true. editTextEMail => " + editTextEMail.getText().toString());

            if (editTextEMail == null || editTextEMail.getText().toString() == "") {
                editTextEMail = findViewById(R.id.editText_email_forget_password);
            }
            presenter.verificationRequestForEMail(editTextEMail.getText().toString());
        }

    }

    //Fields
    private boolean isFullFields() {
        if (editTextEMail.getText().toString().equals("")) {
            Log.d(this.getClass().getSimpleName(), "isFullFields worked. false");
            return false;
        }
        Log.d(this.getClass().getSimpleName(), "isFullFields worked. true");
        return true;
    }//This method checks that the fields are full.

    // E mail //
    private boolean isCorrectEmail() {
        String email = editTextEMail.getText().toString();
        String[] editTextContent = email.split("@");
        if (editTextContent.length == 2) {
            if (editTextContent[0].matches("")) {// @ işaretinden önce bir şey var mı diye kontrol ediyor.
                Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, invalid e mail before '@' => " + (editTextContent[0].equals("") ? "null" : editTextContent[0]));
                ForgetPasswordActivity.this.checkEMail(false);
                return false;
            } else if (editTextContent[1].matches("")) {// @ işaretinden sonra bir şey var mı diye kontrol ediyor.
                Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, invalid e mail after '@' => " + (editTextContent[1].equals("") ? "null" : editTextContent[1]));
                ForgetPasswordActivity.this.checkEMail(false);
                return false;
            } else if (!editTextContent[1].matches("")) {// @ işaretinden sonra ki bir ifade vars onu kontrol eder.

                if (!editTextContent[1].contains(".")) {// @ işaretinden sonra . işareti içeriyor mu ?
                    Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, there's not '.' at e mail after '@' => " + (editTextContent[1].equals("") ? "null" : editTextContent[1]));
                    ForgetPasswordActivity.this.checkEMail(false);
                    return false;
                } else if (editTextContent[1].endsWith(".")) { // @ işaretinden sonra . işaretden sonra bir ifade var mı?
                    Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, there's any nothing at email after '.' => " + (editTextContent[1].equals("") ? "null" : editTextContent[1]));
                    ForgetPasswordActivity.this.checkEMail(false);
                    return false;
                }

            }
            Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => true, invalid e-mail");
            return true;

        }
        Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, invalid e-mail");
        ForgetPasswordActivity.this.checkEMail(false);
        return false;
    }//This method checks the mail field.

    private void checkEMail(boolean eMail) {
        if (eMail) {
            Log.d(this.getClass().getSimpleName(), "checkEMail worked e-mail => true");
            editTextEMail.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.d(this.getClass().getSimpleName(), "checkEMail worked e-mail => false");
            editTextEMail.setText("");
            editTextEMail.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }//This method checks the E-Mail field.


    @Override
    public void verificationResponseForEMail(boolean eMail) {
        Log.d(this.getClass().getSimpleName(), "verificationResponseForEMail worked. e-mail => " + eMail);
        if (eMail) {
            presenter.sendMail(editTextEMail.getText().toString());
        } else {
            Toast.makeText(ForgetPasswordActivity.this, "E mail is invalid.", Toast.LENGTH_SHORT).show();
        }
    }
}

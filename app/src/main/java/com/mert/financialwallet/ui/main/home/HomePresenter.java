package com.mert.financialwallet.ui.main.home;

import android.util.Log;
import android.widget.Toast;

import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.getInfoMoney.ResponseMoneyInfo;
import com.mert.financialwallet.ui.main.MainPresenter;
import com.mert.financialwallet.ui.main.add.AddFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragmentPresenter;

public class HomePresenter extends MainFragmentPresenter implements Home.Presenter, Api.MoneyInfo {
    //Fragment
    private HomeFragment fragment;

    public HomePresenter(MainFragment mainFragment) {
        super(mainFragment);
        fragment = (HomeFragment) mainFragment;
    }

    @Override
    public void getTransactions() {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        }catch (Exception e){
        }
        fragment.mainActivity.dataManager.getMoneyInfo(this);
    }

    @Override
    public void onResponse(ResponseMoneyInfo responseMoneyInfo) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        }catch (Exception e){
        }
        if (responseMoneyInfo.getSuccess()) {
            if (responseMoneyInfo.getMoneyValues() != null) {
                fragment.initView(responseMoneyInfo.getMoneyValues());
            } else {
                fragment.initView(null);
            }
        } else {
            fragment.mainActivity.dataManager.getMoneyInfo(this);
        }

    }

    @Override
    public void onFailure(String responseText) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        }catch (Exception e){
        }

        if (responseText != null){
            Log.i(fragment.mainActivity.getClass().getSimpleName(),responseText);
            Toast.makeText(fragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }
}

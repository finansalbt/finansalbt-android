package com.mert.financialwallet.ui.main.edit;

import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.Main;

import java.util.ArrayList;

public interface Edit {

    interface View extends Main.Fragment.View {
        void clickNumber(int number);

        void clickBackSpace();

        void changeAmountText();

        void showPopUpMenu(ArrayList<Category> categories);
    }

    interface Presenter extends Main.Fragment.Presenter {
        void delete(String transactionId);

        void edit(Transaction transaction);

        void getCategories();
    }


}

package com.mert.financialwallet.ui.main.home;

import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.Main;

import java.util.ArrayList;

public interface Home extends Main.Fragment {
    interface View extends Main.Fragment.View {
    }

    interface Presenter extends Main.Fragment.Presenter {
        void getTransactions();
    }
}

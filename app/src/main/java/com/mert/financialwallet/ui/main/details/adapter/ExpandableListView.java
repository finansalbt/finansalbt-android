package com.mert.financialwallet.ui.main.details.adapter;

import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.Main;
import com.mert.financialwallet.ui.main.details.Details;

public interface ExpandableListView {
    void onChildClick(Transaction transaction);
}
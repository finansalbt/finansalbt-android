package com.mert.financialwallet.ui.router;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;

import com.mert.financialwallet.ui.auth.signin.SignInActivity;
import com.mert.financialwallet.ui.base.activity.BaseActivity;
import com.mert.financialwallet.ui.main.mainActivity.MainActivity;

public class RouterActivity extends BaseActivity implements Router.View {

    private RouterPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(this.getClass().getSimpleName(), "OnCreate worked.");
        presenter = new RouterPresenter(RouterActivity.this);
        Log.i(this.getClass().getSimpleName(), "OnCreate completed.");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i(this.getClass().getSimpleName(), "onResume worked.");

        Log.i(this.getClass().getSimpleName(), "OnCreate completed.");
        if (!RouterActivity.this.dataManager.getLogInMode()) {
            presenter.changeActivity(RouterActivity.this, SignInActivity.class);
            Log.i(this.getClass().getSimpleName(), "go to SignInActivity");
        } else {
            presenter.changeActivity(RouterActivity.this, MainActivity.class);
            Log.i(this.getClass().getSimpleName(), "go to MainActivity");
        }
        RouterActivity.this.finish();

        Log.i(this.getClass().getSimpleName(), "onResume completed.");
    }


}

package com.mert.financialwallet.ui.main.details;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mert.financialwallet.R;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.details.adapter.MoneyExpandableAdapterList;
import com.mert.financialwallet.ui.main.details.model.Header;
import com.mert.financialwallet.ui.main.edit.EditFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.utils.ChangeReceiver.Util.NetworkUtil;
import com.mert.financialwallet.utils.Util;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.segmentedbutton.SegmentedButton;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;

public class DetailsFragment extends MainFragment implements Details.View, com.mert.financialwallet.ui.main.details.adapter.ExpandableListView {
    //Presenter
    private DetailsPresenter presenter;

    //PowerMenu
    private PowerMenu powerMenu;

    //TextView
    @BindView(R.id.textView_no_record_details)
    TextView textViewNoRecord;

    //Expandable ListView
    @BindView(R.id.expandableListView_details)
    ExpandableListView expandableListView;

    private MoneyExpandableAdapterList expandableListAdapter;

    //Back
    @BindView(R.id.imageButton_back_details)
    ImageButton imageButtonBack;

    //BUTTON FOR POP-UP MENU
    @BindView(R.id.button_category_details)
    Button buttonCategory;

    //Radio Group
    @BindView(R.id.radioGroup_currency_details)
    SegmentedButtonGroup radioGroup;
    @BindView(R.id.radioButton_tl_details)
    SegmentedButton radioButtonTl;
    @BindView(R.id.radioButton_dolar_details)
    SegmentedButton radioButtonDolar;
    @BindView(R.id.radioButton_euro_details)
    SegmentedButton radioButtonEuro;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        this.presenter = new DetailsPresenter(DetailsFragment.this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_details, container, false);
        //ButterKnife
        ButterKnife.bind(DetailsFragment.this, view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();


        radioGroup.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int position) {
                Log.d(DetailsFragment.this.getClass().getSimpleName(), "AMOUNT => " + presenter.AMOUNT);

                switch (position) {
                    case 0:
                        radioButtonDolar.setSelected(false);
                        radioButtonEuro.setSelected(false);
                        DetailsFragment.this.presenter.changeAmount(Util.Currency.TURK_LIRA_TEXT);
                        break;
                    case 1:
                        radioButtonTl.setSelected(false);
                        radioButtonEuro.setSelected(false);
                        DetailsFragment.this.presenter.changeAmount(Util.Currency.DOLAR_TEXT);
                        break;
                    case 2:
                        radioButtonDolar.setSelected(false);
                        radioButtonTl.setSelected(false);
                        DetailsFragment.this.presenter.changeAmount(Util.Currency.EURO_TEXT);
                        break;
                }
            }
        });

        buttonCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<Category> categories = new ArrayList<>();

                categories = DetailsFragment.this.presenter.getCategories();

                ArrayList<PowerMenuItem> powerMenuItems = new ArrayList<>();

                if (categories.size() == 0) {
                    Log.i(DetailsFragment.this.getClass().getSimpleName(), "buttonCategory OnClick. Category size == 0");
                    powerMenuItems.add(new PowerMenuItem(Util.CategoryMenu.OTHER, false));

                } else {
                    Log.i(DetailsFragment.this.getClass().getSimpleName(), "buttonCategory OnClick. Category size != 0");
                    for (int i = 0; i < categories.size(); i++) {
                        if (i == 0) {
                            powerMenuItems.add(new PowerMenuItem(categories.get(i).getName(), false));
                        } else {
                            powerMenuItems.add(new PowerMenuItem(categories.get(i).getName(), false));
                        }
                    }
                }

                powerMenu = new PowerMenu.Builder(DetailsFragment.this.getContext())
                        .addItemList(powerMenuItems)
                        .setAnimation(MenuAnimation.DROP_DOWN)
                        .setMenuRadius(10f)
                        .setMenuShadow(10f)
                        .setTextColor(mainActivity.getResources().getColor(R.color.colorTextViewPrimary))
                        .setMenuColor(Color.WHITE).build();

                powerMenu.setOnMenuItemClickListener(new OnMenuItemClickListener<PowerMenuItem>() {
                    @Override
                    public void onItemClick(int position, PowerMenuItem item) {
                        Log.i(DetailsFragment.this.getClass().getSimpleName(), "PowerMenu onItemClick. Item =>" + item.getTitle());

                        buttonCategory.setText(item.getTitle().toUpperCase());

                        presenter.changeCategory(item.getTitle().toUpperCase());

                        if (powerMenu.isShowing()) {
                            powerMenu.dismiss();
                        }
                    }
                });

                powerMenu.showAsDropDown(v);
            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.onBackPressed();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        buttonCategory.setText(presenter.CATEGORY);

        Log.d(this.getClass().getSimpleName(), "onResume Amount => " + presenter.AMOUNT);

        presenter.changeCategory(presenter.CATEGORY);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (powerMenu != null) {
            if (powerMenu.isShowing()) {
                powerMenu.dismiss();
            }
        }
    }

    public void initView(ArrayList<Header> headers) {
        if (NetworkUtil.getState(mainActivity)) {
            if (headers != null) {
                if (!headers.isEmpty()) {
                    buildListeAdapter(headers);
                    hasRecord(true);
                } else {
                    hasRecord(false);
                }
            } else {
                hasRecord(false);
            }
        }
    }

    @Override
    public void stateChanged(boolean isNetwork) {
        super.stateChanged(isNetwork);
        initView();
    }

    public void hasRecord(boolean hasRecord) {
        if (hasRecord) {
            expandableListView.setVisibility(View.VISIBLE);
            textViewNoRecord.setVisibility(View.GONE);
        } else {
            textViewNoRecord.setVisibility(View.VISIBLE);
            expandableListView.setVisibility(View.GONE);
        }
    }

    public void buildListeAdapter(ArrayList<Header> headers) {
        expandableListAdapter = new MoneyExpandableAdapterList(mainActivity, headers, this);
        expandableListView.setAdapter(expandableListAdapter);
    }

    @Override
    public void onChildClick(Transaction transaction) {
        presenter.changeFragment(mainActivity, true, new EditFragment(transaction));
        radioGroup.setPosition(0);
    }
}

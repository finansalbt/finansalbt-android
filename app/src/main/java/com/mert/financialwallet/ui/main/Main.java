package com.mert.financialwallet.ui.main;

import com.mert.financialwallet.ui.base.Base;
import com.mert.financialwallet.ui.main.mainActivity.MainActivity;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;

public interface Main extends Base {

    interface View extends Base.View {

    }

    interface Presenter extends Base.Presenter {
        void changeFragment(MainActivity activity, boolean pushStack, MainFragment mainFragment);
    }

    interface Activity extends Base.Activity {

        interface View extends Base.Activity.View {

        }

        interface Presenter extends Base.Activity.Presenter {
        }

    }

    interface Fragment {

        interface View {
            void initView();
        }

        interface Presenter {
        }

    }
}

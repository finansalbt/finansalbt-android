package com.mert.financialwallet.ui.main.details.adapter;

import android.os.Build;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mert.financialwallet.App;
import com.mert.financialwallet.R;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.details.model.Header;
import com.mert.financialwallet.ui.main.mainActivity.MainActivity;
import com.mert.financialwallet.utils.AmountTextUtil;

import java.util.Date;
import java.util.List;

public class MoneyExpandableAdapterList extends BaseExpandableListAdapter {

    private MainActivity mainActivity;

    private List<Header> headers;

    private ExpandableListView listener;

    public MoneyExpandableAdapterList(MainActivity mainActivity, List<Header> headers, ExpandableListView listener) {
        this.mainActivity = mainActivity;
        this.headers = headers;
        this.listener = listener;
    }

    @Override
    public int getGroupCount() {
        return headers != null ? headers.size() : 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return headers.get(groupPosition).getTransactions().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return headers.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        return headers.get(groupPosition).getTransactions().get(childPosititon);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) mainActivity.getSystemService(mainActivity.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.listview_header, null);
        }

        //Date
        TextView textViewHeader = (TextView) convertView.findViewById(R.id.textView_date_list_header);
        textViewHeader.setText(String.valueOf(headers.get(groupPosition).getHeaderName()));

        //Amount
        TextView textViewAmount = (TextView) convertView.findViewById(R.id.textView_amount_list_header);
        textViewAmount.setText((headers.get(groupPosition).isPositive() ? "+ " : "- ") + new AmountTextUtil(headers.get(groupPosition).getAmount()).getAmountbyString());

        //Row State
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView_row_state_list_header);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (isExpanded) {
                imageView.setImageDrawable(mainActivity.getResources().getDrawable(R.drawable.expand_row, null));
            } else {
                imageView.setImageDrawable(mainActivity.getResources().getDrawable(R.drawable.collapse_row, null));
            }
        } else {
            if (isExpanded) {
                imageView.setImageDrawable(mainActivity.getResources().getDrawable(R.drawable.expand_row));
            } else {
                imageView.setImageDrawable(mainActivity.getResources().getDrawable(R.drawable.collapse_row));
            }
        }

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Log.i("Expandable", "group :" + groupPosition + "child :" + childPosition + " --- " + headers.get(groupPosition).getTransactions().get(childPosition).toString());

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) mainActivity.getSystemService(mainActivity.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.listview_child, null);
        }

        TextView textViewDate = (TextView) convertView.findViewById(R.id.textView_date_listView_child);
        TextView textViewAmount = (TextView) convertView.findViewById(R.id.textView_amount_listView_child);
        ImageButton imageButton = (ImageButton) convertView.findViewById(R.id.imageButton_edit_child);

        //Date
        String createdAt = DateFormat.format("dd", new Date(headers.get(groupPosition).getTransactions().get(childPosition).getCreatedAt())).toString();
        //Amount
        String textAmount = (headers.get(groupPosition).getTransactions().get(childPosition).getType() ? "+ " : "- ") + new AmountTextUtil(headers.get(groupPosition).getTransactions().get(childPosition).getAmount()).getAmountbyString();
        //Category
        String textCategoryName = (App.getUser().getCategoryNamebyId(headers.get(groupPosition).getTransactions().get(childPosition).getCategoryId()));

        textViewAmount.setText(textAmount);

        textViewDate.setText(createdAt);
        textViewDate.setBackgroundColor(headers.get(groupPosition).getTransactions().get(childPosition).getType() ? mainActivity.getResources().getColor(R.color.colorAmountPositive) : mainActivity.getResources().getColor(R.color.colorAmountNegative));

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onChildClick(headers.get(groupPosition).getTransactions().get(childPosition));
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    public List<Header> getHeaders() {
        return headers;
    }
}

package com.mert.financialwallet.ui.auth.forgetPassword;


import com.mert.financialwallet.ui.base.Base;

public interface ForgetPassword extends Base {
    interface View extends Base.View {
        void verificationResponseForEMail(boolean eMail);
    }

    interface Presenter extends Base.Presenter {
        void verificationRequestForEMail(String eMail);

        void sendMail(String eMail);
    }
}

package com.mert.financialwallet.ui.main.mainFragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.mert.financialwallet.ui.main.Main;
import com.mert.financialwallet.ui.main.mainActivity.MainActivity;
import com.mert.financialwallet.utils.ChangeReceiver.OnChangeStateListener;

public class MainFragment extends Fragment implements Main.Fragment.View, OnChangeStateListener {

    private MainFragmentPresenter mainFragmentPresenter;

    public MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mainActivity == null) {
            mainActivity = ((MainActivity) getActivity());
            mainFragmentPresenter = new MainFragmentPresenter(MainFragment.this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void changeFragment(MainActivity activity, boolean pushStack, MainFragment mainFragment) {

    }

    @Override
    public void initView() {

    }

    @Override
    public void stateChanged(boolean isNetwork) {
    }
}

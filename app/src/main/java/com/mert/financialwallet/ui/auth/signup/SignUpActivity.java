package com.mert.financialwallet.ui.auth.signup;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mert.financialwallet.R;
import com.mert.financialwallet.data.model.User;
import com.mert.financialwallet.ui.auth.AuthActivity;
import com.mert.financialwallet.ui.auth.signin.SignInActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SignUpActivity extends AuthActivity implements SignUp.View {

    //Presenter
    private SignUpActivityPresenter presenter;

    //UnBinder
    Unbinder unbinder;

    //Button
    @BindView(R.id.button_sign_up)
    Button buttonSignUp;

    //EditText
    @BindView(R.id.editText_fullName_sign_up)
    EditText editTextFullName;
    @BindView(R.id.editText_userName_sign_up)
    EditText editTextUserName;
    @BindView(R.id.editText_eMail_sign_up)
    EditText editTextEMail;
    @BindView(R.id.editText_password_sign_up)
    EditText editTextPassword;
    @BindView(R.id.editText_re_password_sign_up)
    EditText editTextRePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(this.getClass().getSimpleName(), "onCreateView worked.");
        setContentView(R.layout.activity_sign_up);
        unbinder = ButterKnife.bind(SignUpActivity.this);

        presenter = new SignUpActivityPresenter(SignUpActivity.this);
        presenter.setChangeListener(this, presenter);

        Log.d(this.getClass().getSimpleName(), "onCreateView completed.");
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            registerReceiver(receiver, getIntentFilter());
        }catch (Exception e){

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(receiver);

        }catch (Exception e){

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.button_sign_up)
    void OnClick() {
        if (isCorrectFields()) {
            User user = new User();
            //user.setFullname(editTextFullName.getText().toString());
            user.setFullname("anonymous");
            user.setEMail(editTextEMail.getText().toString());
            user.setUsername(editTextUserName.getText().toString());
            user.setPassword(editTextRePassword.getText().toString());
            presenter.registerRequest(user);
        }
    }

    @Override
    public void registerResponse(boolean response, String reponseText) {
        Log.d(this.getClass().getSimpleName(), "registerResponse worked. response => " + response + "responseText => " + reponseText);
        if (response) {
            Toast.makeText(SignUpActivity.this, "Record is successful!", Toast.LENGTH_SHORT).show();
            SignUpActivity.this.onBackPressed();
        } else {
            Toast.makeText(SignUpActivity.this, reponseText, Toast.LENGTH_SHORT).show();
        }
    }

    //Fields
    private boolean isCorrectFields() {
        clearFields();
        if (!isFullFields()) {
            if (editTextFullName.getText().toString().equals("")) {
                Log.d(this.getClass().getSimpleName(), "field of editTextFullName was empty");
                checkFullName(false);
            }
            if (editTextUserName.getText().toString().equals("")) {
                Log.d(this.getClass().getSimpleName(), "field of editTextUserName was empty");
                checkUserName(false);
            }
            if (editTextEMail.getText().toString().equals("")) {
                Log.d(this.getClass().getSimpleName(), "field of editTextEMail was empty");
                checkEMail(false);
            }
            if (editTextPassword.getText().toString().equals("")) {
                Log.d(this.getClass().getSimpleName(), "field of editTextPassword was empty");
                checkPassword(false);
            }
            if (editTextRePassword.getText().toString().equals("")) {
                Log.d(this.getClass().getSimpleName(), "field of editTextRePassword was empty");
                checkRePassword(false);
            }
            Toast.makeText(SignUpActivity.this, "Fill the empty fields!", Toast.LENGTH_SHORT).show();
        } else if (!isCorrectEMail()) {
            checkEMail(false);
            Toast.makeText(SignUpActivity.this, "Input an e mail!", Toast.LENGTH_SHORT).show();
        } else if (!isCheckPasswords()) {
            checkPassword(false);
            checkRePassword(false);
            Toast.makeText(SignUpActivity.this, "Not the same passwords you entered!", Toast.LENGTH_SHORT).show();
        } else {//All true.
            return true;
        }
        return false;
    }

    private boolean isFullFields() {
        if (editTextFullName.getText().toString().equals("") || editTextUserName.getText().toString().equals("") || editTextEMail.getText().toString().equals("") || editTextPassword.getText().toString().equals("") || editTextRePassword.getText().toString().equals("")) {
            Log.d(this.getClass().getSimpleName(), "isFullFields worked. false");
            return false;
        }
        Log.d(this.getClass().getSimpleName(), "isFullFields worked. true");
        return true;
    }//This method checks that the fields are full.

    private void clearFields() {
        checkFullName(true);
        checkUserName(true);
        checkEMail(true);
        checkPassword(true);
        checkRePassword(true);
    }//This method clears the whole fields.

    private void checkFullName(boolean fullName) {
        if (fullName) {
            Log.d(this.getClass().getSimpleName(), "checkFullName worked fullName => true");
            editTextFullName.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.d(this.getClass().getSimpleName(), "checkFullName worked fullName => false");
            editTextFullName.setText("");
            editTextFullName.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }//This method checks the FullName field.

    private void checkUserName(boolean userName) {
        if (userName) {
            Log.d(this.getClass().getSimpleName(), "checkUserName worked userName => true");
            editTextUserName.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.d(this.getClass().getSimpleName(), "checkUserName worked userName => false");
            editTextUserName.setText("");
            editTextUserName.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }//This method checks the UserName field.


    private void checkEMail(boolean eMail) {
        if (eMail) {
            Log.d(this.getClass().getSimpleName(), "checkEMail worked eMail => true");
            editTextEMail.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.d(this.getClass().getSimpleName(), "checkEMail worked eMail => false");
            editTextEMail.setText("");
            editTextEMail.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }//This method checks the E-Mail field.

    private void checkPassword(boolean password) {
        if (password) {
            Log.d(this.getClass().getSimpleName(), "checkPassword worked eMail => true");
            editTextPassword.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            editTextPassword.setText("");
            Log.d(this.getClass().getSimpleName(), "checkPassword worked eMail => false");
            editTextPassword.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }//This method checks the Password field.

    private void checkRePassword(boolean rePassword) {
        if (rePassword) {
            Log.d(this.getClass().getSimpleName(), "checkRePassword worked eMail => true");
            editTextRePassword.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.d(this.getClass().getSimpleName(), "checkRePassword worked eMail => false");
            editTextRePassword.setText("");
            editTextRePassword.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }//This method checks the RePassword field.

    private void setEditTextBackgroundColor(boolean fullName, boolean userName, boolean eMail, boolean password, boolean rePassword) {
        Log.d(this.getClass().getSimpleName(), "setEditTextBackgroundColor worked fullName =>" + fullName + " userName =>" + userName + " eMail =>" + eMail + " password =>" + password);
        this.checkFullName(fullName);
        this.checkUserName(userName);
        this.checkEMail(eMail);
        this.checkPassword(password);
        this.checkRePassword(rePassword);
    }//This method sets the view's background.

    // E mail //
    private boolean isCorrectEMail() {
        String[] editTextContent = editTextEMail.getText().toString().split("@");
        if (editTextContent.length == 2) {
            if (editTextContent[0].matches("")) {// @ işaretinden önce bir şey var mı diye kontrol ediyor.
                Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, invalid e mail before '@' => " + (editTextContent[0].equals("") ? "null" : editTextContent[0]));
                SignUpActivity.this.checkEMail(false);
                return false;
            } else if (editTextContent[1].matches("")) {// @ işaretinden sonra bir şey var mı diye kontrol ediyor.
                Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, invalid e mail after '@' => " + (editTextContent[1].equals("") ? "null" : editTextContent[1]));
                SignUpActivity.this.checkEMail(false);
                return false;
            } else if (!editTextContent[1].matches("")) {// @ işaretinden sonra ki bir ifade vars onu kontrol eder.

                if (!editTextContent[1].contains(".")) {// @ işaretinden sonra . işareti içeriyor mu ?
                    Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, there's not '.' at e mail after '@' => " + (editTextContent[1].equals("") ? "null" : editTextContent[1]));
                    SignUpActivity.this.checkEMail(false);
                    return false;
                } else if (editTextContent[1].endsWith(".")) { // @ işaretinden sonra . işaretden sonra bir ifade var mı?
                    Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, there's any nothing at email after '.' => " + (editTextContent[1].equals("") ? "null" : editTextContent[1]));
                    SignUpActivity.this.checkEMail(false);
                    return false;
                }

            }
            Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => true, invalid e-mail");
            return true;

        }
        Log.d(this.getClass().getSimpleName(), "isCorrectEmail worked e-mail => false, invalid e-mail");
        SignUpActivity.this.checkEMail(false);
        return false;
    }//This method checks the eMail field.

    // Passwords //
    private boolean isCheckPasswords() {
        if (editTextPassword.getText().toString().equals(editTextRePassword.getText().toString())) {
            Log.d(this.getClass().getSimpleName(), "isCheckPasswords worked eMail => true. That's mean. They are same");
            return true;
        }
        Log.d(this.getClass().getSimpleName(), "isCheckPasswords worked eMail => false. That's mean. They are not same");
        SignUpActivity.this.checkRePassword(false);
        return false;
    }//This method checks the password and re-password are the same.
}

package com.mert.financialwallet.ui.main.edit;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mert.financialwallet.App;
import com.mert.financialwallet.R;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.dialogs.categoryDialog.CategoryDialog;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.utils.AmountTextUtil;
import com.mert.financialwallet.utils.Util;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

@SuppressLint("ValidFragment")
public class EditFragment extends MainFragment implements Edit.View {
    //Transaction
    private Transaction temp;

    //Presenter
    private EditPresenter presenter;

    //UnBinder
    Unbinder unbinder;

    //Text Amount
    public AmountTextUtil amountTextUtil;

    //View
    @BindView(R.id.relativeLayout_view_edit)
    RelativeLayout relativeLayoutView;

    //PowerMenu
    private PowerMenu powerMenu;

    //TopBar
    @BindView(R.id.imageButton_back_edit)
    ImageButton imageButtonBack;
    @BindView(R.id.imageButton_delete_edit)
    ImageButton imageButtonDelete;
    @BindView(R.id.imageButton_check_edit)
    ImageButton imageButtonCheck;
    @BindView(R.id.textView_amount_edit)
    TextView textViewAmount;

    /* Content */

    //Type
    @BindView(R.id.switch_type_edit)
    Switch switchType;
    @BindView(R.id.textView_type_edit)
    TextView textViewType;

    //Currency
    @BindView(R.id.radioGroup_currency_edit)
    RadioGroup radioGroupCurrency;
    @BindView(R.id.radioButton_currency_tl_edit)
    RadioButton radioButtonTl;
    @BindView(R.id.radioButton_currency_dolar_edit)
    RadioButton radioButtonDolar;
    @BindView(R.id.radioButton_currency_euro_edit)
    RadioButton radioButtonEuro;
    //Category
    @BindView(R.id.button_category_edit)
    Button buttonCategory;

    //Comment
    @BindView(R.id.EditText_comment_edit)
    EditText editTextComment;

    /* Keys */
    @BindView(R.id.relativeLayout_keys_edit)
    RelativeLayout relativeLayoutKeys;
    //Numbers
    @BindView(R.id.button_zero_edit)
    Button buttonZero;
    @BindView(R.id.button_one_edit)
    Button buttonOne;
    @BindView(R.id.button_two_edit)
    Button buttonTwo;
    @BindView(R.id.button_three_edit)
    Button buttonThree;
    @BindView(R.id.button_four_edit)
    Button buttonFour;
    @BindView(R.id.button_five_edit)
    Button buttonFive;
    @BindView(R.id.button_six_edit)
    Button buttonSix;
    @BindView(R.id.button_seven_edit)
    Button buttonSeven;
    @BindView(R.id.button_eight_edit)
    Button buttonEight;
    @BindView(R.id.button_nine_edit)
    Button buttonNine;

    //BackSpace
    @BindView(R.id.imageButton_backSpace_edit)
    ImageButton imageButtonBackSpace;

    @SuppressLint("ValidFragment")
    public EditFragment(Transaction temp) {
        this.temp = temp;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit, container, false);
        //ButterKnife
        unbinder = ButterKnife.bind(EditFragment.this, view);

        this.presenter = new EditPresenter(EditFragment.this, temp);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(EditFragment.this.getClass().getSimpleName(), "onViewCreated transaction => " + presenter.transaction.toString());

        this.buttonCategory.setText(App.getUser().getCategoryNamebyId(presenter.transaction.getCategoryId()));

        this.editTextComment.setText(presenter.transaction.getDescription() != null ? presenter.transaction.getDescription() : "");

        this.switchType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (switchType.isChecked()) {
                    presenter.transaction.setType(true);
                    textViewType.setText("Income");
                } else {
                    presenter.transaction.setType(false);
                    textViewType.setText("Expense");
                }
                switchType.getThumbDrawable().setColorFilter(switchType.isChecked() ? mainActivity.getResources().getColor(R.color.colorAmountPositive) : mainActivity.getResources().getColor(R.color.colorAmountNegative), PorterDuff.Mode.MULTIPLY);
            }
        });


        this.amountTextUtil = new AmountTextUtil(presenter.transaction.getAmount());

        this.textViewAmount.setText(amountTextUtil.getAmountbyString());

        this.switchType.setChecked(presenter.transaction.getType());
        this.switchType.getThumbDrawable().setColorFilter(switchType.isChecked() ? mainActivity.getResources().getColor(R.color.colorAmountPositive) : mainActivity.getResources().getColor(R.color.colorAmountNegative), PorterDuff.Mode.MULTIPLY);
        this.textViewType.setText(presenter.transaction.getType() ? "Income" : "Expense");

        if (presenter.transaction.getCurrencyUnit().toUpperCase().equals(Util.Currency.TURK_LIRA_TEXT)) {
            this.radioButtonTl.setChecked(true);
        } else if (presenter.transaction.getCurrencyUnit().toUpperCase().equals(Util.Currency.DOLAR_TEXT)) {
            this.radioButtonDolar.setChecked(true);
        } else if (presenter.transaction.getCurrencyUnit().toUpperCase().equals(Util.Currency.EURO_TEXT)) {
            this.radioButtonEuro.setChecked(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.onBackPressed();
            }
        });

        imageButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.delete(presenter.transaction.getId());
            }
        });
        imageButtonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amountTextUtil.getAmountbyDouble() <= 0) {
                    Toast.makeText(mainActivity, "Please, entry the amount!", Toast.LENGTH_SHORT).show();
                } else {
                    presenter.transaction = new Transaction(
                            presenter.transaction.getId(),
                            switchType.isChecked(),
                            amountTextUtil.getAmountbyDouble(),
                            getWhichChecked(),
                            App.getUser().getCategorytIdbyName(buttonCategory.getText().toString().toUpperCase()),
                            editTextComment.getText() == null ? "" : editTextComment.getText().toString()
                    );

                    presenter.edit(presenter.transaction);
                }

            }
        });

        relativeLayoutView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (relativeLayoutView != null) {
                    int heightDiff = relativeLayoutView.getRootView().getHeight() - relativeLayoutView.getHeight();
                    if (heightDiff > Util.Constants.dpToPx(mainActivity, 200)) { // if more than 200 dp, it's probably a keyboard...
                        relativeLayoutKeys.setVisibility(View.GONE);
                    } else {
                        relativeLayoutKeys.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

    }

    private String getWhichChecked() {
        if (radioButtonTl.isChecked()) {
            return Util.Currency.TURK_LIRA_TEXT;
        } else if (radioButtonDolar.isChecked()) {
            return Util.Currency.DOLAR_TEXT;
        } else if (radioButtonEuro.isChecked()) {
            return Util.Currency.EURO_TEXT;
        } else {
            return Util.Currency.TURK_LIRA_TEXT;
        }
    }

    @OnClick(R.id.button_category_edit)
    public void onShowCategories(View view) {
        EditFragment.this.presenter.getCategories();
    }

    @OnClick(R.id.imageButton_check_edit)
    public void onCheckClick(View v) {
        if (textViewAmount.getText().toString().equals("00,00")) {
            Toast.makeText(mainActivity, "", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick({R.id.button_zero_edit, R.id.button_one_edit, R.id.button_two_edit, R.id.button_three_edit, R.id.button_four_edit, R.id.button_five_edit, R.id.button_six_edit, R.id.button_seven_edit, R.id.button_eight_edit, R.id.button_nine_edit})
    public void onNumbersClick(View view) {

        if (amountTextUtil.getAmountbyString().length() < 14) {
            switch (view.getId()) {
                case R.id.button_zero_edit:
                    this.clickNumber(0);
                    break;
                case R.id.button_one_edit:
                    this.clickNumber(1);
                    break;
                case R.id.button_two_edit:
                    this.clickNumber(2);
                    break;
                case R.id.button_three_edit:
                    this.clickNumber(3);
                    break;
                case R.id.button_four_edit:
                    this.clickNumber(4);
                    break;
                case R.id.button_five_edit:
                    this.clickNumber(5);
                    break;
                case R.id.button_six_edit:
                    this.clickNumber(6);
                    break;
                case R.id.button_seven_edit:
                    this.clickNumber(7);
                    break;
                case R.id.button_eight_edit:
                    this.clickNumber(8);
                    break;
                case R.id.button_nine_edit:
                    this.clickNumber(9);
                    break;
                default:
                    break;
            }
        }
    }

    @OnClick(R.id.imageButton_backSpace_edit)
    public void onBackSpace(View view) {
        this.clickBackSpace();
    }

    @Override
    public void clickNumber(int number) {
        amountTextUtil.add(number);
        this.changeAmountText();
    }

    @Override
    public void clickBackSpace() {
        this.changeAmountText();
        textViewAmount.setText(amountTextUtil.backSpace());
    }

    @Override
    public void changeAmountText() {
        textViewAmount.setText(amountTextUtil.getAmountbyString());
    }

    @Override
    public void showPopUpMenu(ArrayList<Category> categories) {
        ArrayList<PowerMenuItem> powerMenuItems = new ArrayList<>();

        for (int i = 0; i < categories.size(); i++) {
            powerMenuItems.add(new PowerMenuItem(categories.get(i).getName(), false));
        }
        powerMenuItems.add(new PowerMenuItem(Util.CategoryMenu.CATEGORY_ADD, false));

        powerMenu = new PowerMenu.Builder(EditFragment.this.getContext())
                .addItemList(powerMenuItems)
                .setAnimation(MenuAnimation.DROP_DOWN)
                .setMenuRadius(10f)
                .setMenuShadow(10f)
                .setTextColor(mainActivity.getResources().getColor(R.color.colorTextViewPrimary))
                .setMenuColor(Color.WHITE).build();

        powerMenu.setOnMenuItemClickListener(new OnMenuItemClickListener<PowerMenuItem>() {
            @Override
            public void onItemClick(int position, PowerMenuItem item) {
                Log.i(EditFragment.this.getClass().getSimpleName(), "PowerMenu onItemClick. Item =>" + item.getTitle());

                if (powerMenu.isShowing()) {
                    powerMenu.dismiss();
                }

                if (item.getTitle() == Util.CategoryMenu.CATEGORY_ADD) {

                    CategoryDialog categoryDialog = new CategoryDialog()
                            .setListener(presenter)
                            .setActivity(mainActivity);

                    FragmentManager fm = mainActivity.getSupportFragmentManager();
                    categoryDialog.show(fm, "dialog_category");

                } else {
                    buttonCategory.setText(item.getTitle().toUpperCase());
                }

            }
        });

        powerMenu.showAsDropDown(buttonCategory);
    }

    @Override
    public void initView() {

    }

    @Override
    public void stateChanged(boolean isNetwork) {
        super.stateChanged(isNetwork);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (powerMenu != null) {
            if (powerMenu.isShowing()) {
                powerMenu.dismiss();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}

package com.mert.financialwallet.ui.main.home;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.charts.Cartesian3d;
import com.anychart.core.axes.Linear;
import com.anychart.core.cartesian.series.Bar;
import com.anychart.data.Mapping;
import com.anychart.data.Set;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.LabelsOverlapMode;
import com.anychart.enums.Orientation;
import com.anychart.enums.ScaleStackMode;
import com.anychart.enums.TooltipDisplayMode;
import com.anychart.enums.TooltipPositionMode;

import com.mert.financialwallet.App;
import com.mert.financialwallet.R;
import com.mert.financialwallet.data.model.MoneyValue;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.add.AddFragment;
import com.mert.financialwallet.ui.main.details.DetailsFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.ui.main.settings.SettingsFragment;
import com.mert.financialwallet.utils.AmountTextUtil;
import com.mert.financialwallet.utils.ChangeReceiver.Util.NetworkUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HomeFragment extends MainFragment implements Home.View {
    //Presenter
    private HomePresenter presenter;

    //UnBinder
    Unbinder unbinder;

    //TopBar
    @BindView(R.id.textView_username_home)
    TextView textViewUserName;
    @BindView(R.id.imageButton_settings_home)
    ImageButton imageButtonSettings;

    //Floating Action Button
    @BindView(R.id.fab_add_home)
    FloatingActionButton fabAdd;

    //Budget Table
    @BindView(R.id.textView_tl_budget__home)
    TextView textViewTlBudget;
    @BindView(R.id.textView_dolar_budget_home)
    TextView textViewDolarBudget;
    @BindView(R.id.textView_euro_budget_home)
    TextView textViewEuroBudget;

    //Chart
    @BindView(R.id.relativeLayout_details_budget_home)
    RelativeLayout relativeLayoutDetails;
    @BindView(R.id.anyChart_home)
    AnyChartView anyChartView;

    //Button
    @BindView(R.id.button_details_home)
    Button buttonDetails;

    //No Transaction TextView
    @BindView(R.id.textView_no_record_home)
    TextView textViewNoTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        //ButterKnife
        unbinder = ButterKnife.bind(HomeFragment.this, view);

        this.presenter = new HomePresenter(HomeFragment.this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(HomeFragment.this.getClass().getSimpleName(), "onStart worked.");

        try {
            textViewUserName.setText(App.getUser().getUsername());
        } catch (Exception e) {

        }

        if (NetworkUtil.getState(mainActivity)) {
            presenter.getTransactions();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        relativeLayoutDetails.setVisibility(View.GONE);
        buttonDetails.setVisibility(View.GONE);
        textViewNoTransaction.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.imageButton_settings_home)
    void onClickSettings() {
        presenter.changeFragment(mainActivity, true, new SettingsFragment());
    }

    @OnClick(R.id.fab_add_home)
    void onClickFab() {
        presenter.changeFragment(mainActivity, true, new AddFragment());
    }

    @OnClick(R.id.button_details_home)
    void onClickDetails() {
        presenter.changeFragment(mainActivity, true, new DetailsFragment());
    }

    @Override
    public void stateChanged(boolean isNetwork) {
        super.stateChanged(isNetwork);
        mainActivity.stateChanged(isNetwork);
        if (isNetwork) {
            this.initView();
        }
    }

    public void initView(MoneyValue moneyValue) {
        if (moneyValue != null) {
            Log.d(HomeFragment.this.getClass().getSimpleName(), "initView moneyValue => null değil.");

            if (moneyValue.getTRY() < 0) {
                textViewTlBudget.setText("-" + new AmountTextUtil(moneyValue.getTRY()).getAmountbyString());
            } else if (moneyValue.getTRY() == 0) {
                textViewTlBudget.setText(" " + new AmountTextUtil(moneyValue.getTRY()).getAmountbyString());
            } else {
                textViewTlBudget.setText("+" + new AmountTextUtil(moneyValue.getTRY()).getAmountbyString());
            }

            if (moneyValue.getUSD() < 0) {
                textViewDolarBudget.setText("-" + new AmountTextUtil(moneyValue.getUSD()).getAmountbyString());
            } else if (moneyValue.getUSD() == 0) {
                textViewDolarBudget.setText(" " + new AmountTextUtil(moneyValue.getUSD()).getAmountbyString());
            } else {
                textViewDolarBudget.setText("+" + new AmountTextUtil(moneyValue.getUSD()).getAmountbyString());
            }

            if (moneyValue.getEUR() < 0) {
                textViewEuroBudget.setText("-" + new AmountTextUtil(moneyValue.getEUR()).getAmountbyString());
            } else if (moneyValue.getEUR() == 0) {
                textViewEuroBudget.setText(" " + new AmountTextUtil(moneyValue.getEUR()).getAmountbyString());
            } else {
                textViewEuroBudget.setText("+" + new AmountTextUtil(moneyValue.getEUR()).getAmountbyString());
            }

            if (moneyValue.getTRYIncome() != 0 || moneyValue.getUSDIncome() != 0 || moneyValue.getEURIncome() != 0 || moneyValue.getTRYExpense() != 0 || moneyValue.getUSDExpense() != 0 || moneyValue.getEURExpense() != 0) {


                Cartesian3d bar3d = AnyChart.bar3d();

                bar3d.animation(true);

                bar3d.yScale().minimum(0d);

                bar3d.yAxis(0d).labels().format("{%Value}{groupsSeparator: }");

                List<DataEntry> data = new ArrayList<>();
                data.add(new CustomDataEntry("TRY", moneyValue.getTRYIncome(), moneyValue.getTRYExpense()));
                data.add(new CustomDataEntry("USD", moneyValue.getUSDIncome(), moneyValue.getUSDExpense()));
                data.add(new CustomDataEntry("EUR", moneyValue.getEURIncome(), moneyValue.getEURExpense()));

                Set set = Set.instantiate();
                set.data(data);
                Mapping bar1Data = set.mapAs("{ x: 'x', value: 'value' }");
                Mapping bar2Data = set.mapAs("{ x: 'x', value: 'value2' }");

                bar3d.bar(bar1Data)
                        .name("Income").color("Green");

                bar3d.bar(bar2Data)
                        .name("Expense").color("Red").labels(false);

                bar3d.legend().enabled(true);
                bar3d.legend().fontSize(13d);
                bar3d.legend().padding(0d, 0d, 16d, 0d);

                bar3d.interactivity().hoverMode(HoverMode.SINGLE);

                bar3d.tooltip()
                        .positionMode(TooltipPositionMode.FLOAT)
                        .position("right")
                        .anchor(Anchor.LEFT_CENTER)
                        .offsetX(4d)
                        .offsetY(0d)
                        .format("{%Value}");

                bar3d.zAspect("25%")
                        .zPadding(16d)
                        .zAngle(48d)
                        .zDistribution(true);

                anyChartView.setOnRenderedListener(new AnyChartView.OnRenderedListener() {
                    @Override
                    public void onRendered() {
                        relativeLayoutDetails.setVisibility(View.VISIBLE);
                        textViewNoTransaction.setVisibility(View.GONE);
                        buttonDetails.setVisibility(View.VISIBLE);
                    }
                });
                anyChartView.setChart(bar3d);


            } else {
                relativeLayoutDetails.setVisibility(View.GONE);
                buttonDetails.setVisibility(View.GONE);
                textViewNoTransaction.setVisibility(View.VISIBLE);
            }


        } else {
            Log.d(HomeFragment.this.getClass().getSimpleName(), "initView moneyValue => null.");

            relativeLayoutDetails.setVisibility(View.GONE);
            buttonDetails.setVisibility(View.GONE);
            textViewNoTransaction.setVisibility(View.VISIBLE);
        }
    }

    private class CustomDataEntry extends ValueDataEntry {
        CustomDataEntry(String x, Number value, Number value2) {
            super(x, value);
            setValue("value2", value2);
        }
    }
}

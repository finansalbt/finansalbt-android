package com.mert.financialwallet.ui.main.details.model;

import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.utils.AmountTextUtil;

import java.util.ArrayList;
import java.util.List;


public class Header {

    private String HeaderName;
    private List<Transaction> transactions;

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public boolean isPositive() {
        if (transactions.size() > 0) {
            double amount = 0;
            for (int i = 0; i < transactions.size(); i++) {
                if (transactions.get(i).getType()){
                    amount += transactions.get(i).getAmount();
                }else {
                    amount -= transactions.get(i).getAmount();
                }
            }

            if (amount > 0) {
                return true;
            } else {
                return false;
            }

        } else {
            return true;
        }
    }

    public double getAmount() {
        if (transactions.size() > 0) {
            double amount = 0;

            for (int i = 0; i < transactions.size(); i++) {
                if (transactions.get(i).getType()){
                    amount += transactions.get(i).getAmount();
                }else {
                    amount -= transactions.get(i).getAmount();
                }
            }

            AmountTextUtil amountTextUtil = new AmountTextUtil(amount);

            return amountTextUtil.getAmountbyDouble();

        } else {
            return 0.00d;
        }
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public String getHeaderName() {
        return HeaderName;
    }

    public void setHeaderName(String HeaderName) {
        this.HeaderName = HeaderName;
    }

    public void addTrasaction(Transaction run) {
        if (transactions == null) {
            transactions = new ArrayList<>();
        }

        transactions.add(run);
    }
}

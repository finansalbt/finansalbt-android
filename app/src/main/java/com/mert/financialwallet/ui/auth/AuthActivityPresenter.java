package com.mert.financialwallet.ui.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mert.financialwallet.App;
import com.mert.financialwallet.ui.dialogs.DialogManager;
import com.mert.financialwallet.ui.main.mainActivity.MainActivity;
import com.mert.financialwallet.utils.ChangeReceiver.ChangeBroadcastReceiver;
import com.mert.financialwallet.utils.ChangeReceiver.OnChangeStateListener;
import com.mert.financialwallet.utils.Util;

public class AuthActivityPresenter implements Auth.Activity.Presenter {

    private AuthActivity authActivity;

    public AuthActivityPresenter(AuthActivity authActivity) {
        this.authActivity = authActivity;
    }

    @Override
    public void changeActivity(Activity activity, Class to) {
        Log.i(this.getClass().getSimpleName(), "changeActivity  => to " + to.toString() + " from " + activity.toString());
        Intent intent = new Intent(activity, to);
        intent.putExtra(Util.Constants.PREVIOUS_ACTIVITY, activity.getClass().getSimpleName());
        activity.startActivity(intent);
    }

    protected void setChangeListener(Activity activity, OnChangeStateListener listener) {
        authActivity.receiver = new ChangeBroadcastReceiver(activity, listener);
    }
}

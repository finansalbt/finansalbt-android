package com.mert.financialwallet.ui.base.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;

import com.mert.financialwallet.R;
import com.mert.financialwallet.ui.base.Base;
import com.mert.financialwallet.ui.base.BasePresenter;
import com.mert.financialwallet.ui.dialogs.DialogManager;
import com.mert.financialwallet.utils.ChangeReceiver.OnChangeStateListener;
import com.mert.financialwallet.utils.Util;

public class BaseActivityPresenter extends BasePresenter implements Base.Presenter, Base.Activity.Presenter, OnChangeStateListener {
    protected BaseActivity baseActivity;

    public BaseActivityPresenter(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Override
    public void changeActivity(Activity activity, Class to) {
        Log.i(this.getClass().getSimpleName(), "changeActivity  => to " + to.getClass().getSimpleName() + " from " + activity.getClass().getSimpleName());
        Intent intent = new Intent(activity, to);
        intent.putExtra(Util.Constants.PREVIOUS_ACTIVITY, activity.getClass().getSimpleName());
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void buildDialog(Context context) {
        baseActivity.dialogManager = new DialogManager(context);
    }


    @Override
    public void stateChanged(boolean isNetwork) {

    }
}

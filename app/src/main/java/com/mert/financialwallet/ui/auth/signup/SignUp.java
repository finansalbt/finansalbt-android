package com.mert.financialwallet.ui.auth.signup;

import com.mert.financialwallet.data.model.User;
import com.mert.financialwallet.ui.auth.Auth;

public interface SignUp extends Auth {

    interface View extends Auth.View {
        void registerResponse(boolean response, String reponseText);
    }

    interface Presenter extends Auth.Presenter {
        void registerRequest(User user);
    }
}

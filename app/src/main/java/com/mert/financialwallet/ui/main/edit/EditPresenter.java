package com.mert.financialwallet.ui.main.edit;

import android.util.Log;
import android.widget.Toast;

import com.mert.financialwallet.App;
import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.dialogs.categoryDialog.CategorDialog;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragmentPresenter;
import com.mert.financialwallet.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class EditPresenter extends MainFragmentPresenter implements Edit.Presenter, CategorDialog, Api.AddCategory, Api.GetCategories, Api.DeleteTransaction, Api.EditTransaction {

    //Transaction
    public static Transaction transaction;

    //Fragment
    private EditFragment fragment;

    public EditPresenter(MainFragment mainFragment, Transaction transaction) {
        super(mainFragment);

        fragment = (EditFragment) mainFragment;

        if (transaction != null) {
            this.transaction = transaction;
        } else {
            this.transaction = new Transaction(true, 0.00d, Util.Currency.TURK_LIRA_TEXT, App.getUser().getCategorytIdbyName("OTHER"), App.getUser().getCategorytIdbyName(Util.CategoryMenu.OTHER));
        }

    }

    @Override
    public void delete(String transactionId) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        } catch (Exception e) {
        }

        fragment.mainActivity.dataManager.deleteTransaction(transactionId, this);
    }

    @Override
    public void edit(Transaction transaction) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        } catch (Exception e) {
        }
        fragment.mainActivity.dataManager.editTransaction(transaction, this);
    }

    @Override
    public void getCategories() {
        fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        fragment.mainActivity.dataManager.getCategories(this);
    }

    @Override
    public void newCategory(String category) {
        Log.i(this.getClass().getSimpleName(), "newCategory Category =>" + category);
        Category temp = new Category();
        temp.setName(category);
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
        } catch (Exception e) {
        }
        fragment.mainActivity.dataManager.addCategory(temp, this);
    }

    //GetCategory
    @Override
    public void onResponse(boolean isTrue, List<Category> categories) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }
        ArrayList<Category> categoryArrayList = new ArrayList<>();
        if (categories.isEmpty()) {
            Log.i(this.getClass().getSimpleName(), "onResponse empty => " + categories.isEmpty());
            Toast.makeText(fragment.mainActivity, "Categories couldn't find!", Toast.LENGTH_SHORT).show();
        } else {
            for (Category category : categories) {
                Log.i(this.getClass().getSimpleName(), "onResponse categoryId => " + category.getId());
                categoryArrayList.add(category);
            }
        }

        if (isTrue) {
            if (!categoryArrayList.isEmpty()) {
                App.getUser().setCategories(categoryArrayList);
                fragment.showPopUpMenu(categoryArrayList);
            } else {
                Toast.makeText(fragment.mainActivity, "Categories couldn't find!", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(fragment.mainActivity, "Categories couldn't find!", Toast.LENGTH_SHORT).show();
        }
    }

    //AddCategory
    @Override
    public void onResponse(boolean isTrue, Category category, String responseText) {
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }

        if (isTrue && category != null) {
            try {
                fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).show();
            } catch (Exception e) {
            }
            fragment.mainActivity.dataManager.getCategories(this);
        } else {
            Toast.makeText(fragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }

    //Delete-Edit Transaction
    @Override
    public void onResponse(boolean isTrue, String responseText) {
        Log.i(this.getClass().getSimpleName(), "onResponse isTrue => " + isTrue + " responseText => " + responseText);
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }

        if (isTrue) {
            Toast.makeText(fragment.mainActivity, "Successful!", Toast.LENGTH_SHORT).show();
            fragment.mainActivity.onBackPressed();
        } else {
            if (responseText != null) {

                Toast.makeText(fragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
            } else {
            }
        }
    }

    //ResponseText
    @Override
    public void onFailure(String responseText) {
        Log.i(this.getClass().getSimpleName(), "onFailure responseText => " + responseText);
        try {
            fragment.mainActivity.dialogManager.getProgressDialog(fragment.mainActivity).dismiss();
        } catch (Exception e) {
        }

        if (responseText != null) {
            Toast.makeText(fragment.mainActivity, responseText, Toast.LENGTH_SHORT).show();
        }
    }


}

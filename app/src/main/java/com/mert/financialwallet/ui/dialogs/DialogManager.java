package com.mert.financialwallet.ui.dialogs;

import android.content.Context;
import android.util.Log;

import com.mert.financialwallet.utils.Manager;

public  class DialogManager extends Manager {

    //Close Dialog
    private CloseDialog closeDialog;

    //Progress Dialog
    private ProgressDialog progressDialog;

    //Internet Dialog
    private InternetDialog internetDialog;

    public DialogManager(Context context) {
        super(context);
        Log.i(this.getClass().getSimpleName(), "Constructor worked.");

        Log.i(this.getClass().getSimpleName(), "Constructor completed.");
    }

    public CloseDialog getCloseDialog() {
        Log.i(this.getClass().getSimpleName(), "getCloseDialog.");

        if (closeDialog == null) {
            Log.i(this.getClass().getSimpleName(), "closeDialog is null.");
            closeDialog = new CloseDialog(context);
        }

        return closeDialog;
    }

    public ProgressDialog getProgressDialog(Context context) {
        Log.i(this.getClass().getSimpleName(), "getProgressDialog.");
        if (progressDialog == null) {
            Log.i(this.getClass().getSimpleName(), "progressDialog is null.");
            progressDialog = new ProgressDialog(context);
        }

        return progressDialog;
    }

    public InternetDialog getInternetDialog(Context context) {
        Log.i(this.getClass().getSimpleName(), "getInternetDialog.");
        if (internetDialog == null) {
            Log.i(this.getClass().getSimpleName(), "internetDialog is null.");
            internetDialog = new InternetDialog(context);
        }

        return internetDialog;
    }
}

package com.mert.financialwallet.ui.main.add;

import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.Main;

import java.util.ArrayList;

public interface Add extends Main.Fragment {

    interface View extends Main.Fragment.View {
        void clickNumber(int number);

        void clickBackSpace();

        void changeAmountText();

        void showPopUpMenu(ArrayList<Category> categories);
    }

    interface Presenter extends Main.Fragment.Presenter {
        void check(Transaction transaction);

        void getCategories();
    }
}

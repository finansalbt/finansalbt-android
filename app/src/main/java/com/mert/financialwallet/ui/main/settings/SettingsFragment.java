package com.mert.financialwallet.ui.main.settings;

import android.graphics.Color;
import android.media.audiofx.Equalizer;
import android.os.Bundle;
import android.support.design.card.MaterialCardView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mert.financialwallet.R;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.ui.auth.signin.SignInActivity;
import com.mert.financialwallet.ui.main.details.DetailsFragment;
import com.mert.financialwallet.ui.main.home.HomeFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.utils.ApiUtil;
import com.mert.financialwallet.utils.Util;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import java.util.ArrayList;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SettingsFragment extends MainFragment implements Settings.View {
    //Presenter
    SettingsPresenter presenter;

    //UnBinder
    Unbinder unbinder;

    //Top Bar
    @BindView(R.id.imageButton_back_settings)
    ImageButton imageButtonBack;

    //Log Out
    @BindView(R.id.cardView_delete_category_settings)
    MaterialCardView cardViewDeleteCardView;

    //Delete Category
    @BindView(R.id.cardView_logout_settings)
    MaterialCardView cardViewLogOut;
    //PowerMenu
    private PowerMenu powerMenu;

    //Password
    @BindView(R.id.cardView_password_settings)
    MaterialCardView cardViewPassword;
    @BindView(R.id.linearLayout_password_change_settings)
    LinearLayout linearLayout;
    @BindView(R.id.button_password_change_settings)
    Button buttonPasswordChange;
    @BindView(R.id.editText_password_change_settings)
    EditText editTextPasswordChange;
    @BindView(R.id.editText_re_password_change_settings)
    EditText editTextRePasswordChange;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(SettingsFragment.this, view);

        presenter = new SettingsPresenter(SettingsFragment.this);

        return view;
    }

    @OnClick(R.id.button_password_change_settings)
    public void ChangeButton(View view) {
        checkPassword(true);
        checkRePassword(true);
        if (editTextPasswordChange.getText().toString().matches("") && editTextRePasswordChange.getText().toString().matches("")) {
            checkPassword(false);
            checkRePassword(false);
            Toast.makeText(mainActivity, "Fill the empty fields!", Toast.LENGTH_SHORT).show();
        } else if (editTextPasswordChange.getText().toString().matches("")) {
            checkPassword(false);
            Toast.makeText(mainActivity, "Fill the empty fields!", Toast.LENGTH_SHORT).show();
        } else if (editTextRePasswordChange.getText().toString().matches("")) {
            checkRePassword(false);
            Toast.makeText(mainActivity, "Fill the empty fields!", Toast.LENGTH_SHORT).show();
        } else {
            if (!editTextPasswordChange.getText().toString().equals(editTextRePasswordChange.getText().toString())) {
                checkPassword(false);
                checkRePassword(false);
                Toast.makeText(mainActivity, "Password fields must be the same!", Toast.LENGTH_SHORT).show();
            } else {
                checkPassword(true);
                checkRePassword(true);
                presenter.sendPassword(editTextRePasswordChange.getText().toString());
            }
        }
    }

    @OnClick(R.id.cardView_password_settings)
    public void PasswordChangeisShow(View view) {
        linearLayout.setVisibility(linearLayout.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
    }

    @OnClick(R.id.imageButton_back_settings)
    public void BackPressed(View view) {
        mainActivity.onBackPressed();
    }

    @OnClick(R.id.cardView_logout_settings)
    public void LogOut(View view) {
        mainActivity.presenter.changeActivity(mainActivity, SignInActivity.class);
        mainActivity.finish();
    }

    @OnClick(R.id.cardView_delete_category_settings)
    public void deleteCardView(View view) {
        ArrayList<Category> categories = new ArrayList<>();

        categories = SettingsFragment.this.presenter.getCategories();

        ArrayList<PowerMenuItem> powerMenuItems = new ArrayList<>();

        Log.i(SettingsFragment.this.getClass().getSimpleName(), "buttonCategory OnClick. Category size != 0");
        for (int i = 0; i < categories.size(); i++) {
            powerMenuItems.add(new PowerMenuItem(categories.get(i).getName(), false));
        }

        powerMenu = new PowerMenu.Builder(SettingsFragment.this.getContext())
                .addItemList(powerMenuItems)
                .setAnimation(MenuAnimation.DROP_DOWN)
                .setMenuRadius(10f)
                .setMenuShadow(10f)
                .setTextColor(mainActivity.getResources().getColor(R.color.colorTextViewPrimary))
                .setMenuColor(Color.WHITE).build();

        powerMenu.setOnMenuItemClickListener(new OnMenuItemClickListener<PowerMenuItem>() {
            @Override
            public void onItemClick(int position, PowerMenuItem item) {
                String categoryId = SettingsFragment.this.presenter.getCategories().get(position).getId();
                String categoryName = SettingsFragment.this.presenter.getCategories().get(position).getName();
                Log.i(SettingsFragment.this.getClass().getSimpleName(), "PowerMenu onItemClick. Item =>" + item.getTitle() + " categoryId => " + categoryId);

                if (SettingsFragment.this.presenter.getCategories().size() > 1) {
                    if (!categoryName.toUpperCase().equals(Util.CategoryMenu.OTHER)) {
                        presenter.deleteCategory(categoryId);
                    } else {
                        Toast.makeText(SettingsFragment.this.mainActivity, "OTHER Category can't be deleted!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SettingsFragment.this.mainActivity, "It can't be deleted!", Toast.LENGTH_SHORT).show();
                }


                if (powerMenu.isShowing()) {
                    powerMenu.dismiss();
                }

            }
        });

        if (!powerMenuItems.isEmpty()) {
            powerMenu.showAtCenter(SettingsFragment.this.getView());
        }else {
            Toast.makeText(SettingsFragment.this.mainActivity, "Couldn't find category!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (powerMenu != null) {
            if (powerMenu.isShowing()) {
                powerMenu.dismiss();
            }
        }
    }

    private void checkPassword(boolean password) {
        if (password) {
            Log.d(this.getClass().getSimpleName(), "checkEMail worked e-mail => true");
            editTextPasswordChange.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.d(this.getClass().getSimpleName(), "checkEMail worked e-mail => false");
            editTextPasswordChange.setText("");
            editTextPasswordChange.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }//This method checks the Password field.

    private void checkRePassword(boolean rePassword) {
        if (rePassword) {
            Log.d(this.getClass().getSimpleName(), "checkEMail worked e-mail => true");
            editTextRePasswordChange.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.d(this.getClass().getSimpleName(), "checkEMail worked e-mail => false");
            editTextRePasswordChange.setText("");
            editTextRePasswordChange.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }//This method checks the RePassword field.


    @Override
    public void showChangeLayout(boolean state) {
        checkPassword(true);
        checkRePassword(true);

        editTextRePasswordChange.setText("");
        editTextPasswordChange.setText("");

        linearLayout.setVisibility(state ? View.VISIBLE : View.GONE);
    }
}

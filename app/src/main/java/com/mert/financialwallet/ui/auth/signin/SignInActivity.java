package com.mert.financialwallet.ui.auth.signin;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mert.financialwallet.R;
import com.mert.financialwallet.ui.auth.AuthActivity;
import com.mert.financialwallet.ui.auth.forgetPassword.ForgetPasswordActivity;
import com.mert.financialwallet.ui.auth.forgetPassword.ForgetPasswordActivityPresenter;
import com.mert.financialwallet.ui.auth.signup.SignUpActivity;
import com.mert.financialwallet.ui.main.mainActivity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SignInActivity extends AuthActivity implements SignIn.View {

    //Presenter
    private SignInActivityPresenter presenter;

    //UnBinder
    Unbinder unbinder;

    //Button
    @BindView(R.id.button_sign_in)
    Button buttonSignIn;

    //EditText
    @BindView(R.id.editText_userName_sign_in)
    EditText editTextUserName;
    @BindView(R.id.editText_password_sign_in)
    EditText editTextPassword;

    @BindView(R.id.button_create_sign_in)
    Button buttonCreateAccount;
    @BindView(R.id.button_forget_sign_in)
    Button buttonForget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(this.getClass().getSimpleName(), "onCreateView worked.");
        setContentView(R.layout.activity_sign_in);
        unbinder = ButterKnife.bind(SignInActivity.this);

        presenter = new SignInActivityPresenter(SignInActivity.this);
        presenter.setChangeListener(this, presenter);

        if (dataManager.getSharedPreferences().getLoginMode()) {
            dataManager.clearLocal();
        }

        Log.i(this.getClass().getSimpleName(), "onCreateView completed.");
    }


    @Override
    protected void onStart() {
        super.onStart();
        try{
            registerReceiver(receiver, getIntentFilter());
        }catch (Exception e){

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            unregisterReceiver(receiver);

        }catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter = new SignInActivityPresenter(SignInActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.button_sign_in)
    void OnClickSignIn() {
        Log.i(this.getClass().getSimpleName(), "OnClickSignIn pressed.");
        if (isCorrectFields()) {
            presenter.verificationRequestForUser(editTextUserName.getText().toString(), editTextPassword.getText().toString());
        }
    }

    @OnClick(R.id.button_create_sign_in)
    void OnClickCreate() {
        Log.i(this.getClass().getSimpleName(), "OnClickCreate pressed.");
        presenter.changeActivity(SignInActivity.this, SignUpActivity.class);
    }

    @OnClick(R.id.button_forget_sign_in)
    void OnClickForget() {
        Log.i(this.getClass().getSimpleName(), "OnClickForget pressed.");
        presenter.changeActivity(SignInActivity.this, ForgetPasswordActivity.class);
    }

    //View Init
    private boolean isCorrectFields() {
        checkUserName(true);
        checkPassword(true);
        Log.i(this.getClass().getSimpleName(), "buttonSignIn pressed.");
        if (!isFullFields()) {
            if (editTextUserName.getText().toString().matches("")) {
                Log.i(this.getClass().getSimpleName(), "field of editTextUserName was empty");
                checkUserName(false);
            }
            if (editTextPassword.getText().toString().matches("")) {
                Log.i(this.getClass().getSimpleName(), "field of editTextPassword was empty");
                checkPassword(false);
            }
            Toast.makeText(SignInActivity.this, "Fill the empty fields!", Toast.LENGTH_SHORT).show();
        } else {//All true.
            Log.i(this.getClass().getSimpleName(), "View completed.");
            return true;
        }
        return false;
    }

    //Verification
    @Override
    public void verificationResponseForUser(boolean response, String reponseText) {
        Log.i(this.getClass().getSimpleName(), "verificationResponseForUser worked response =>" + response);

        if (response) {
            Log.i(this.getClass().getSimpleName(), "verificationResponseForUser worked. Both was true");
            presenter.changeActivity(SignInActivity.this, MainActivity.class);
            SignInActivity.this.finish();
        } else {
            Toast.makeText(SignInActivity.this, reponseText, Toast.LENGTH_SHORT).show();
            clearFields();
        }
    }

    //Fields
    private boolean isFullFields() {
        if (editTextUserName.getText().toString().equals("") || editTextPassword.getText().toString().equals("")) {
            Log.i(this.getClass().getSimpleName(), "isFullFields worked. It was false");
            return false;
        }
        Log.i(this.getClass().getSimpleName(), "isFullFields worked. It was true");
        return true;
    }//This method checks that the fields are full.

    private void clearFields() {
        Log.i(this.getClass().getSimpleName(), "clearFields worked. All cleared.");
        this.setEditTextBackgroundColor(true, true);

        this.editTextUserName.setText("");
        this.editTextPassword.setText("");
    }//This method clears the whole fields.

    private void checkUserName(boolean userName) {
        if (userName) {
            Log.i(this.getClass().getSimpleName(), "checkUserName worked userName => true");
            editTextUserName.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.i(this.getClass().getSimpleName(), "checkUserName worked userName => false");
            editTextUserName.setText("");
            editTextUserName.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }

    private void checkPassword(boolean password) {
        if (password) {
            Log.i(this.getClass().getSimpleName(), "checkPassword worked password => true");
            editTextPassword.setBackgroundResource(R.drawable.shape_edittext_correct);
        } else {
            Log.i(this.getClass().getSimpleName(), "checkPassword worked password => false");
            editTextPassword.setText("");
            editTextPassword.setBackgroundResource(R.drawable.shape_edittext_incorrect);
        }
    }

    private void setEditTextBackgroundColor(boolean userName, boolean password) {
        Log.i(this.getClass().getSimpleName(), "setEditTextBackgroundColor worked userName =>" + userName + " password =>" + password);
        this.checkUserName(userName);
        this.checkPassword(password);
    }//This method sets the view's background.

}

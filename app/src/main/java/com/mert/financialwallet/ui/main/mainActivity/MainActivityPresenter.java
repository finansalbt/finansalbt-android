package com.mert.financialwallet.ui.main.mainActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mert.financialwallet.App;
import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.db.api.Retrofit.Retrofit;
import com.mert.financialwallet.data.model.User;
import com.mert.financialwallet.ui.auth.signin.SignInActivity;
import com.mert.financialwallet.ui.dialogs.DialogManager;
import com.mert.financialwallet.ui.main.Main;
import com.mert.financialwallet.ui.main.MainPresenter;
import com.mert.financialwallet.ui.main.home.HomeFragment;
import com.mert.financialwallet.ui.main.mainFragment.MainFragment;
import com.mert.financialwallet.utils.ChangeReceiver.OnChangeStateListener;

public class MainActivityPresenter extends MainPresenter implements Main.Activity.Presenter, Api.SignIn, OnChangeStateListener {

    private MainActivity mainActivity;

    public boolean flag;

    public MainActivityPresenter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        flag = false;
    }

    @Override
    public void changeActivity(Activity activity, Class to) {
        Log.i(this.getClass().getSimpleName(), "changeActivity  => to " + to.toString() + " from " + activity.toString());
        Intent intent = new Intent(activity, to);
        activity.startActivity(intent);
    }

    @Override
    public void buildDialog(Context context) {
        mainActivity.dialogManager = new DialogManager(context);
    }

    @Override
    public void changeFragment(MainActivity activity, boolean pushStack, MainFragment mainFragment) {
        mainActivity.changeFragment(mainActivity, pushStack, mainFragment);
    }

    @Override
    public void onResponse(boolean isTrue, User user, String responseText) {
        Log.i(this.getClass().getSimpleName(), "onResponse worked. responseText => " + responseText + " user => " + user != null ? user.toString() : "null" + " isTrue => " + isTrue);
        try {
            mainActivity.dialogManager.getProgressDialog(mainActivity).dismiss();
        } catch (Exception e) {
        }

        if (user != null) {
            App.setUser(user);

            if (!flag) {
                this.changeFragment(mainActivity, false, new HomeFragment());
                flag = true;
            }

        } else {
            mainActivity.presenter.changeActivity(mainActivity, SignInActivity.class);
            mainActivity.finish();
        }
    }

    @Override
    public void onFailure(String responseText) {
        Log.i(this.getClass().getSimpleName(), "onFailure worked. responseText => " + responseText);
        try {
            mainActivity.dialogManager.getProgressDialog(mainActivity).dismiss();
        } catch (Exception e) {
        }

        mainActivity.presenter.changeActivity(mainActivity, SignInActivity.class);
        mainActivity.finish();
    }

    @Override
    public void stateChanged(boolean isNetwork) {
        if (isNetwork) {
            try {
                mainActivity.dialogManager.getInternetDialog(mainActivity).dismiss();
            } catch (Exception e) {

            }
            mainActivity.initApp();
        } else {
            try {
                mainActivity.dialogManager.getInternetDialog(mainActivity).show();
            } catch (Exception e) {

            }
        }
    }
}

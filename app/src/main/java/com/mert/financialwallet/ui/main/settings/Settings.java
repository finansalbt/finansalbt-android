package com.mert.financialwallet.ui.main.settings;

import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.main.Main;

import java.util.ArrayList;

public interface Settings extends Main.Fragment {
    interface View extends Main.Fragment.View {
        void showChangeLayout(boolean state);
    }

    interface Presenter extends Main.Fragment.Presenter {
        ArrayList<Category> getCategories();

        void sendPassword(String password);

        void deleteCategory(String categoryId);
    }
}

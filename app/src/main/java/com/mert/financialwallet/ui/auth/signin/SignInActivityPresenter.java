package com.mert.financialwallet.ui.auth.signin;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.db.api.Retrofit.Retrofit;
import com.mert.financialwallet.data.model.User;
import com.mert.financialwallet.ui.auth.AuthActivity;
import com.mert.financialwallet.ui.auth.AuthActivityPresenter;
import com.mert.financialwallet.utils.ChangeReceiver.OnChangeStateListener;
import com.mert.financialwallet.utils.ChangeReceiver.Util.NetworkUtil;

public class SignInActivityPresenter extends AuthActivityPresenter implements SignIn.Presenter, Api.SignIn,OnChangeStateListener {
    private SignInActivity activity;

    public SignInActivityPresenter(SignInActivity activity) {
        super((AuthActivity) activity);
        this.activity = activity;
        Log.i(this.getClass().getSimpleName(), "SignInActivityPresenter created");
    }

    @Override
    public void changeActivity(Activity activity, Class to) {
        super.changeActivity(activity, to);
    }

    @Override
    public void verificationRequestForUser(String userName, String password) {
        Log.i(this.getClass().getSimpleName(), "verificationRequestForUser worked userName =>" + userName + " password =>" + password);
        if (NetworkUtil.getState(activity)) {
            try {
                activity.dialogManager.getProgressDialog(activity).show();
            }catch (Exception e){

            }
            activity.dataManager.signIn(userName, password, this);
        } else {
            Toast.makeText(activity, "Internetinizi açınız !", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onResponse(boolean isTrue, User user,String responseText) {
        Log.i(this.getClass().getSimpleName(), "onResponse worked.");
        try {
            activity.dialogManager.getProgressDialog(activity).dismiss();
        }catch (Exception e){

        }

        if (responseText != null) {
            Toast.makeText(activity, responseText, Toast.LENGTH_SHORT).show();
        }

        activity.verificationResponseForUser(isTrue, responseText);
    }

    @Override
    public void onFailure(String responseText) {
        Log.i(this.getClass().getSimpleName(), "onFailure worked.");
        try {
            activity.dialogManager.getProgressDialog(activity).dismiss();
        }catch (Exception e){

        }

        if (responseText != null) {
            Toast.makeText(activity, responseText, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void setChangeListener(Activity activity, OnChangeStateListener listener) {
        super.setChangeListener(activity, listener);
    }

    @Override
    public void stateChanged(boolean isNetwork) {
        if (isNetwork) {
            try {
                activity.dialogManager.getInternetDialog(activity).dismiss();
            } catch (Exception e) {

            }
        } else {
            try {
                activity.dialogManager.getInternetDialog(activity).show();
            } catch (Exception e) {

            }
        }
    }
}

package com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteTransaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyDeleteTransaction {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;

    /**
     * No args constructor for use in serialization
     *
     */
    public BodyDeleteTransaction() {
    }

    /**
     *
     * @param transactionId
     * @param userId
     */
    public BodyDeleteTransaction(String userId, String transactionId) {
        super();
        this.userId = userId;
        this.transactionId = transactionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "BodyDeleteTransaction{" +
                "userId='" + userId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                '}';
    }
}

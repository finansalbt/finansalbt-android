package com.mert.financialwallet.data;

import android.app.Activity;
import android.util.Log;

import com.mert.financialwallet.App;
import com.mert.financialwallet.data.db.api.Api;
import com.mert.financialwallet.data.db.api.ApiHelper;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.changePassword.BodyChangePassword;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.base.activity.BaseActivity;
import com.mert.financialwallet.data.db.sharedPreferences.SharedPreferencesHelper;
import com.mert.financialwallet.data.model.User;
import com.mert.financialwallet.utils.Manager;

import java.util.ArrayList;

public class DataManager extends Manager implements Api.Local {
    //Activity
    private Activity activity;

    /* Helpers */

    //SharedPreferencesHelper
    private SharedPreferencesHelper sharedPreferencesHelper;

    //FireBaseHelper
    private ApiHelper apiHelper;

    private static boolean ourInstance = false;

    public DataManager(Activity activity) {
        super(activity);
        Log.i(this.getClass().getSimpleName(), "constructor worked.");

        this.activity = activity;

        this.getSharedPreferences();

        this.getAPIHelper();

        Log.i(this.getClass().getSimpleName(), "constructor completed.");
    }

    //Sign
    public void signIn(String userName, String password, Api.SignIn listener) {
        Log.i(this.getClass().getSimpleName(), "signIn userName => " + userName + " password => " + password + " listener => " + listener.toString());
        apiHelper.signIn(userName, password, listener);
    }

    public void signUp(User user, Api.SignUp listener) {
        Log.i(this.getClass().getSimpleName(), "signUp User => " + user.toString() + " listener => " + listener.toString());
        apiHelper.signUp(user, listener);
    }

    public void signOut() {
        Log.i(this.getClass().getSimpleName(), "SignOut");
        this.clearLocal();
    }

    public void initUser(Api.SignIn listener) {
        Log.i(this.getClass().getSimpleName(), "initUser userName=> " + sharedPreferencesHelper.getUserName() + " password => " + sharedPreferencesHelper.getPassword() + " listener => " + listener.toString());
        apiHelper.signIn(sharedPreferencesHelper.getUserName(), sharedPreferencesHelper.getPassword(), listener);
    }

    //builds
    public void buildSharedPreferences() {
        if (sharedPreferencesHelper == null) {
            Log.i(this.getClass().getSimpleName(), "buildSharedPreferences => build");
            sharedPreferencesHelper = new SharedPreferencesHelper((BaseActivity) activity);
        }
    }

    public void buildAPIHelper() {
        if (apiHelper == null) {
            Log.i(this.getClass().getSimpleName(), "buildAPIHelper => build");
            apiHelper = new ApiHelper((BaseActivity) activity, this);
        }
    }


    //Getters
    public SharedPreferencesHelper getSharedPreferences() {
        this.buildSharedPreferences();
        return this.sharedPreferencesHelper;
    }

    public ApiHelper getAPIHelper() {
        this.buildAPIHelper();
        return this.apiHelper;
    }


    //Logged
    public Boolean getLogInMode() {
        Log.i(this.getClass().getSimpleName(), "getLogInMode => " + getSharedPreferences().getLoginMode());
        return getSharedPreferences().getLoginMode();
    }

    //Local
    public void signInLocal(User user) {
        Log.i(this.getClass().getSimpleName(), "signInLocal => user" + user.toString());
        getSharedPreferences().setLoginMode(true);
        getSharedPreferences().setId(user.getId());
        getSharedPreferences().setEMail(user.getEMail());

        if (user.getPassword() != null) {
            getSharedPreferences().setPassword(user.getPassword());
        }

        getSharedPreferences().setFullName(user.getFullname());
        getSharedPreferences().setUserName(user.getUsername());
    }

    public void clearLocal() {
        Log.i(this.getClass().getSimpleName(), "clearLocal");
        getSharedPreferences().clear();
    }

    @Override
    public void signIn(User user) {
        if (user == null) {
            Log.i(this.getClass().getSimpleName(), "signIn user => null");
            this.clearLocal();
            App.setUser(null);
        } else {
            Log.i(this.getClass().getSimpleName(), "signIn user => " + user.toString());
            App.setUser(user);
            signInLocal(user);
        }
    }

    @Override
    public void changePassword(String password) {
        sharedPreferencesHelper.setPassword(password);
    }

    @Override
    public void deleteCategory(String categoryId) {
        Category temp = null;
        for (Category category : App.getUser().getCategories()) {
            if (category.getId().equals(categoryId)) {
                temp = category;
            }
        }

        if (temp != null) {
            App.getUser().getCategories().remove(temp);

            ArrayList<Transaction> transactionArrayList = new ArrayList<>();
            for (Transaction transaction : App.getUser().getTransactions()) {
                if (transaction.getCategoryId().equals(categoryId)) {
                    transactionArrayList.add(transaction);
                }
            }

            for (Transaction transaction : transactionArrayList) {
                App.getUser().getTransactions().remove(transaction);
            }
        }
    }

    //Get
    public void getCategories(Api.GetCategories listener) {
        apiHelper.getCategories(sharedPreferencesHelper.getId(), listener);
    }

    public void getTransactions(String categoryId, Api.GetTransactions listener) {
        apiHelper.getTransactions(sharedPreferencesHelper.getId(), categoryId, listener);
    }

    //Add
    public void addCategory(Category category, final Api.AddCategory listener) {
        apiHelper.addCategory(category, sharedPreferencesHelper.getId(), listener);
    }

    public void addTransaction(Transaction transaction, final Api.AddTransaction listener) {
        apiHelper.addTransaction(transaction, sharedPreferencesHelper.getId(), listener);
    }

    //Password
    public void changePassword(String newPassword, final Api.ChangePassword listener) {

        apiHelper.changePassword(new BodyChangePassword(
                        sharedPreferencesHelper.getId(), sharedPreferencesHelper.getPassword(), newPassword),
                listener);
    }

    public void forgetPassword(String email, final Api.ForgetPassword listener) {
        apiHelper.forgetPassword(email, listener);
    }

    //Money Info
    public void getMoneyInfo(Api.MoneyInfo listener) {
        apiHelper.getMoney(sharedPreferencesHelper.getId(), listener);
    }

    //Delete Transaction
    public void deleteTransaction(String transactionId, Api.DeleteTransaction listener) {
        apiHelper.deleteTransaction(sharedPreferencesHelper.getId(), transactionId, listener);
    }

    public void editTransaction(Transaction transaction, Api.EditTransaction listener) {
        apiHelper.editTransaction(sharedPreferencesHelper.getId(), transaction, listener);
    }

    public void deleteCategory(String categoryId, Api.DeleteCategory listener) {
        apiHelper.deleteCategory(sharedPreferencesHelper.getId(), categoryId, listener);
    }
}

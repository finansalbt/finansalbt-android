package com.mert.financialwallet.data.db.api.Retrofit.postModel.editTransaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseEditTransaction {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("msg")
    @Expose
    private Integer msg;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseEditTransaction() {
    }

    /**
     *
     * @param msg
     * @param success
     */
    public ResponseEditTransaction(Boolean success, Integer msg) {
        super();
        this.success = success;
        this.msg = msg;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getMsg() {
        return msg;
    }

    public void setMsg(Integer msg) {
        this.msg = msg;
    }


}

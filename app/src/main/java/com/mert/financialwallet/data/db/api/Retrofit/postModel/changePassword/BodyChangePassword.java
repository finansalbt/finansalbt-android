package com.mert.financialwallet.data.db.api.Retrofit.postModel.changePassword;

import com.mert.financialwallet.data.model.Transaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyChangePassword {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("old_password")
    @Expose
    public String oldPassword;
    @SerializedName("new_password")
    @Expose
    public String newPassword;

    /**
     * No args constructor for use in serialization
     *
     */
    public BodyChangePassword() {
    }

    /**
     *
     * @param newPassword
     * @param userId
     * @param oldPassword
     */
    public BodyChangePassword(String userId, String oldPassword, String newPassword) {
        super();
        this.userId = userId;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

}

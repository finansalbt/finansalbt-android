package com.mert.financialwallet.data.db.api.Retrofit.postModel.addTransactions;

import com.mert.financialwallet.data.model.Transaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyAddTransaction {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("transaction")
    @Expose
    private Transaction transaction;

    /**
     * No args constructor for use in serialization
     *
     */
    public BodyAddTransaction() {
    }

    /**
     *
     * @param transaction
     * @param userId
     */
    public BodyAddTransaction(String userId, Transaction transaction) {
        super();
        this.userId = userId;
        this.transaction = transaction;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}

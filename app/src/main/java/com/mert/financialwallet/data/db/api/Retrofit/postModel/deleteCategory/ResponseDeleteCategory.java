package com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteCategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseDeleteCategory {
    @SerializedName("success")
    @Expose
    private Boolean success;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseDeleteCategory() {
    }

    /**
     *
     * @param success
     */
    public ResponseDeleteCategory(Boolean success) {
        super();
        this.success = success;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}

package com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteCategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyDeleteCategory {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;

    /**
     * No args constructor for use in serialization
     *
     */
    public BodyDeleteCategory() {
    }

    /**
     *
     * @param categoryId
     * @param userId
     */
    public BodyDeleteCategory(String userId, String categoryId) {
        super();
        this.userId = userId;
        this.categoryId = categoryId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "BodyDeleteCategory{" +
                "userId='" + userId + '\'' +
                ", categoryId='" + categoryId + '\'' +
                '}';
    }
}

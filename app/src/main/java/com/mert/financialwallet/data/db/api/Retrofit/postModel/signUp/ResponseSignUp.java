package com.mert.financialwallet.data.db.api.Retrofit.postModel.signUp;

import android.support.annotation.Nullable;

import com.mert.financialwallet.data.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseSignUp {

    @SerializedName("success")
    private Boolean success;

    @SerializedName("user")
    @Nullable
    private User user;

    @SerializedName("msg")
    @Nullable
    private String msg;

    public ResponseSignUp(Boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public User getUser() {
        return user;
    }

    public void setUser(@Nullable User user) {
        this.user = user;
    }
}

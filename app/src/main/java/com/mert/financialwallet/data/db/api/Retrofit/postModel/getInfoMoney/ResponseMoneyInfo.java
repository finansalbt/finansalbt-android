package com.mert.financialwallet.data.db.api.Retrofit.postModel.getInfoMoney;

import android.support.annotation.Nullable;

import com.mert.financialwallet.data.model.MoneyValue;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ResponseMoneyInfo {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("money_values")
    @Expose
    private List<MoneyValue> moneyValues = null;

    /**
     * No args constructor for use in serialization
     */
    public ResponseMoneyInfo() {
    }

    /**
     * @param moneyValues
     * @param success
     */
    public ResponseMoneyInfo(Boolean success, List<MoneyValue> moneyValues) {
        super();
        this.success = success;
        this.moneyValues = moneyValues;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public MoneyValue getMoneyValues() {
        if (moneyValues.size() > 0) {
            return moneyValues.get(0);
        } else {
            return null;
        }
    }

    public void setMoneyValues(MoneyValue moneyValue) {
        if (this.moneyValues != null) {
            this.moneyValues.clear();
        } else {
            this.moneyValues = new List<MoneyValue>() {
                @Override
                public int size() {
                    return 0;
                }

                @Override
                public boolean isEmpty() {
                    return false;
                }

                @Override
                public boolean contains(Object o) {
                    return false;
                }


                @Override
                public Iterator<MoneyValue> iterator() {
                    return null;
                }


                @Override
                public Object[] toArray() {
                    return new Object[0];
                }

                @Override
                public <T> T[] toArray(T[] a) {
                    return null;
                }

                @Override
                public boolean add(MoneyValue moneyValue) {
                    return false;
                }

                @Override
                public boolean remove(Object o) {
                    return false;
                }

                @Override
                public boolean containsAll(Collection<?> c) {
                    return false;
                }

                @Override
                public boolean addAll(Collection<? extends MoneyValue> c) {
                    return false;
                }

                @Override
                public boolean addAll(int index, Collection<? extends MoneyValue> c) {
                    return false;
                }

                @Override
                public boolean removeAll(Collection<?> c) {
                    return false;
                }

                @Override
                public boolean retainAll(Collection<?> c) {
                    return false;
                }

                @Override
                public void clear() {

                }

                @Override
                public MoneyValue get(int index) {
                    return null;
                }

                @Override
                public MoneyValue set(int index, MoneyValue element) {
                    return null;
                }

                @Override
                public void add(int index, MoneyValue element) {

                }

                @Override
                public MoneyValue remove(int index) {
                    return null;
                }

                @Override
                public int indexOf(Object o) {
                    return 0;
                }

                @Override
                public int lastIndexOf(Object o) {
                    return 0;
                }


                @Override
                public ListIterator<MoneyValue> listIterator() {
                    return null;
                }


                @Override
                public ListIterator<MoneyValue> listIterator(int index) {
                    return null;
                }


                @Override
                public List<MoneyValue> subList(int fromIndex, int toIndex) {
                    return null;
                }
            };
        }
        this.moneyValues.add(moneyValue);
    }

    @Override
    public String toString() {
        return "ResponseMoneyInfo{" +
                "success=" + success +
                ", moneyValues=" + moneyValues +
                '}';
    }
}

package com.mert.financialwallet.data.db.api.Retrofit.postModel.addCategories;

import android.support.annotation.Nullable;

import com.mert.financialwallet.data.model.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseAddCategory {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("category")
    @Nullable
    @Expose
    private Category category;

    @SerializedName("msg")
    @Nullable
    @Expose
    private String msg;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseAddCategory() {
    }

    /**
     *
     * @param msg
     * @param success
     */
    public ResponseAddCategory(Boolean success, String msg) {
        super();
        this.success = success;
        this.msg = msg;
    }

    public ResponseAddCategory(Boolean success, Category category) {
        super();
        this.success = success;
        this.category = category;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


}

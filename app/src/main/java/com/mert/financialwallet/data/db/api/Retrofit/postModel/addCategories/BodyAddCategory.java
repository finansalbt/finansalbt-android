package com.mert.financialwallet.data.db.api.Retrofit.postModel.addCategories;

import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyAddCategory {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("category")
    @Expose
    private Category category;

    /**
     * No args constructor for use in serialization
     *
     */
    public BodyAddCategory() {
    }

    /**
     *
     * @param category
     * @param userId
     */
    public BodyAddCategory(String userId, Category category) {
        super();
        this.userId = userId;
        this.category = category;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}

package com.mert.financialwallet.data.db.api.Retrofit.postModel.getTransactions;

import android.support.annotation.Nullable;

import com.mert.financialwallet.data.model.Transaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetTransactions {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("transactions")
    @Nullable
    @Expose
    private List<Transaction> transactions = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseGetTransactions() {
    }

    /**
     *
     * @param transactions
     * @param success
     */
    public ResponseGetTransactions(Boolean success, List<Transaction> transactions) {
        super();
        this.success = success;
        this.transactions = transactions;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

}

package com.mert.financialwallet.data.db.sharedPreferences;


import android.app.Activity;
import android.content.Context;
import android.util.Log;

public class SharedPreferencesHelper {
    private static final String MY_PREFS = "my_prefs";
    private static final String ID = "id";
    private static final String E_MAIL = "eMail";
    private static final String FULL_NAME = "fullName";
    private static final String USER_NAME = "userName";
    private static final String PASSWORD = "password";
    private static final String LOGIN_MODE = "login_mode";

    private android.content.SharedPreferences preferences;
    private android.content.SharedPreferences.Editor editor;

    //Activity
    private Activity activity;

    public SharedPreferencesHelper(Activity activity) {
        this.activity = activity;
        this.preferences = this.activity.getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);
        this.editor = this.preferences.edit();

        this.toString();
    }

    //Id
    public String getId() {
        String id = preferences.getString(ID, "-1");
        Log.i(this.getClass().getSimpleName(), "getId, -id :" + id);
        return id;
    }

    public boolean setId(String id) {
        Log.i(this.getClass().getSimpleName(), "setId, -id :" + id);
        return editor.putString(ID, id).commit();
    }

    //User Name
    public String getEMail() {
        String eMail = preferences.getString(E_MAIL, "");
        Log.i(this.getClass().getSimpleName(), "getEMail, -eMail:" + eMail);
        return eMail;
    }

    public boolean setEMail(String eMail) {
        Log.i(this.getClass().getSimpleName(), "setEMail, -eMail :" + eMail);
        return editor.putString(E_MAIL, eMail).commit();
    }

    //FULL NAME
    public String getFullName() {
        String fullName = preferences.getString(FULL_NAME, "");
        Log.i(this.getClass().getSimpleName(), "getFullName, -fullName:" + fullName);
        return fullName;
    }

    public boolean setFullName(String fullName) {
        Log.i(this.getClass().getSimpleName(), "setFullName, -fullName :" + fullName);
        return editor.putString(USER_NAME, fullName).commit();
    }

    //USER NAME
    public String getUserName() {
        String userName = preferences.getString(USER_NAME, "");
        Log.i(this.getClass().getSimpleName(), "getUserName, -userName:" + userName);
        return userName;
    }

    public boolean setUserName(String userName) {
        Log.i(this.getClass().getSimpleName(), "setUserName, -userName :" + userName);
        return editor.putString(USER_NAME, userName).commit();
    }

    //PASSWORD
    public String getPassword() {
        String password = preferences.getString(PASSWORD, "");
        Log.i(this.getClass().getSimpleName(), "getPassword, -password:" + password);
        return password;
    }

    public boolean setPassword(String password) {
        Log.i(this.getClass().getSimpleName(), "setPassword, -password :" + password);
        return editor.putString(PASSWORD, password).commit();
    }

    //LOGIN MODE
    public boolean setLoginMode(boolean loginMode) {
        Log.i(this.getClass().getSimpleName(), "Login Mode =>" + loginMode);
        return editor.putBoolean(LOGIN_MODE, loginMode).commit();
    }

    public boolean getLoginMode() {
        boolean isLogin = preferences.getBoolean(LOGIN_MODE, false);
        Log.i(this.getClass().getSimpleName(), "Login Mode =>" + isLogin);
        return isLogin;
    }

    public boolean clear() {
        Log.i(this.getClass().getSimpleName(), "clear");
        return editor.clear().commit();
    }

    @Override
    public String toString() {
        String turnText = "SharedPreferencesHelper toString=>\n id = " + getId() + "\n email =>" + getEMail() + "\n password => " + getPassword() + "\n fullName => " + getFullName() + "\n userName => " + getUserName() + "\n loginMode => " + getLoginMode();
        Log.i(this.getClass().getSimpleName(), turnText);
        return turnText;
    }
}

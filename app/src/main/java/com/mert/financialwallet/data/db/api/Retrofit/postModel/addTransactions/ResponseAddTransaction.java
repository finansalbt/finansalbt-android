package com.mert.financialwallet.data.db.api.Retrofit.postModel.addTransactions;

import android.support.annotation.Nullable;

import com.mert.financialwallet.data.model.Transaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseAddTransaction {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("transaction")
    @Nullable
    @Expose
    private Transaction transaction;

    @SerializedName("msg")
    @Nullable
    @Expose
    private String msg;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseAddTransaction() {
    }

    /**
     *
     * @param msg
     * @param success
     */
    public ResponseAddTransaction(Boolean success, String msg) {
        super();
        this.success = success;
        this.msg = msg;
    }

    public ResponseAddTransaction(Boolean success, Transaction transaction) {
        super();
        this.success = success;
        this.transaction = transaction;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return "ResponseAddTransaction{" +
                "success=" + success +
                ", transaction=" + transaction +
                ", msg='" + msg + '\'' +
                '}';
    }
}

package com.mert.financialwallet.data.db.api.Retrofit;

import com.mert.financialwallet.data.db.api.Retrofit.postModel.addCategories.BodyAddCategory;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.addCategories.ResponseAddCategory;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.addTransactions.BodyAddTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.addTransactions.ResponseAddTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.changePassword.BodyChangePassword;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.changePassword.ResponseChangePassword;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteCategory.BodyDeleteCategory;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteCategory.ResponseDeleteCategory;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteTransaction.BodyDeleteTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteTransaction.ResponseDeleteTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.editTransaction.BodyEditTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.editTransaction.ResponseEditTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.forgetPassword.ResponseForgetPassword;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.getCategories.ResponseGetCategories;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.getInfoMoney.ResponseMoneyInfo;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.getTransactions.ResponseGetTransactions;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.signIn.BodySignIn;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.signIn.ResponseSignIn;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.signUp.BodySignUp;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.signUp.ResponseSignUp;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Retrofit {

    @POST("category/add")
    Call<ResponseAddCategory> addCategory(@Body BodyAddCategory addCategory);

    @POST("transaction/add")
    Call<ResponseAddTransaction> addTransaction(@Body BodyAddTransaction addTransaction);

    @POST("user/register")
    Call<ResponseSignUp> signUp(@Body BodySignUp signUp);

    @POST("user/login")
    Call<ResponseSignIn> signIn(@Body BodySignIn signIn);

    @GET("transaction/gettransactions/{user_id}/{category_id}")
    Call<ResponseGetTransactions> getTransactions(@Path("user_id") String user_id,
                                                  @Path("category_id") String category_id);

    @GET("user/getcategories/{user_id}")
    Call<ResponseGetCategories> getCategories(@Path("user_id") String user_id);

    @PATCH("user/changepassword")
    Call<ResponseChangePassword> changePassword(@Body BodyChangePassword changePassword);

    @GET("user/forgetpassword/{email_address}")
    Call<ResponseForgetPassword> forgetPassword(@Path("email_address") String email_address);

    @GET("user/getmoneyinfo/{user_id}")
    Call<ResponseMoneyInfo> getMoneyInfo(@Path("user_id") String user_id);

    @HTTP(method = "DELETE", path = "user/deletetransaction", hasBody = true)
    Call<ResponseDeleteTransaction> deleteTransaction(@Body BodyDeleteTransaction deleteTransaction);

    @HTTP(method = "DELETE", path = "user/deletecategory", hasBody = true)
    Call<ResponseDeleteCategory> deleteCategory(@Body BodyDeleteCategory deleteCategory);

    @PUT("transaction/edit")
    Call<ResponseEditTransaction> editTransaction(@Body BodyEditTransaction editTransaction);
}

package com.mert.financialwallet.data.db.api;

import android.util.Log;

import com.mert.financialwallet.data.db.api.Retrofit.RetrofitClient;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.addCategories.BodyAddCategory;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.addCategories.ResponseAddCategory;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.addTransactions.BodyAddTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.addTransactions.ResponseAddTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.changePassword.BodyChangePassword;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.changePassword.ResponseChangePassword;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteCategory.BodyDeleteCategory;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteCategory.ResponseDeleteCategory;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteTransaction.BodyDeleteTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteTransaction.ResponseDeleteTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.editTransaction.BodyEditTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.editTransaction.ResponseEditTransaction;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.forgetPassword.ResponseForgetPassword;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.getCategories.ResponseGetCategories;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.getInfoMoney.ResponseMoneyInfo;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.getTransactions.ResponseGetTransactions;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.signIn.BodySignIn;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.signIn.ResponseSignIn;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.signUp.BodySignUp;
import com.mert.financialwallet.data.db.api.Retrofit.postModel.signUp.ResponseSignUp;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.ui.base.activity.BaseActivity;
import com.mert.financialwallet.data.model.User;
import com.mert.financialwallet.utils.ApiUtil;
import com.mert.financialwallet.utils.Util;

import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiHelper implements Api {

    //Activity
    private BaseActivity baseActivity;

    //DataManager
    public Api.Local localListener;

    public ApiHelper(BaseActivity baseActivity, Api.Local localListener) {
        Log.i(ApiHelper.this.getClass().getSimpleName(), "ApiHelper worked.");
        this.baseActivity = baseActivity;

        this.localListener = localListener;

        Log.i(ApiHelper.this.getClass().getSimpleName(), "ApiHelper completed.");
    }

    //Login
    public void signUp(User user, final Api.SignUp listener) {
        Log.i(this.getClass().getSimpleName(), "signUp User =>" + user.toString());

        Call<ResponseSignUp> call = RetrofitClient
                .getInstance()
                .getApi()
                .signUp(new BodySignUp(user.getFullname(), user.getEMail(), user.getUsername(), user.getPassword()));

        call.enqueue(new Callback<ResponseSignUp>() {
            @Override
            public void onResponse(Call<ResponseSignUp> call, Response<ResponseSignUp> response) {
                ResponseSignUp responseSignUp = response.body();

                if (responseSignUp.getSuccess()) {
                    listener.onResponse(responseSignUp.getSuccess(), null);
                } else {
                    listener.onResponse(false, ApiUtil.convert(String.valueOf(responseSignUp.getMsg())));
                }
            }

            @Override
            public void onFailure(Call<ResponseSignUp> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });

    }

    public void signIn(String username, final String sifre, final Api.SignIn listener) {
        Log.i(ApiHelper.this.getClass().getSimpleName(), "signIn userName=>" + username + " sifre =>" + sifre);

        Call<ResponseSignIn> call = RetrofitClient
                .getInstance()
                .getApi()
                .signIn(new BodySignIn(username, sifre));

        call.enqueue(new Callback<ResponseSignIn>() {
            @Override
            public void onResponse(Call<ResponseSignIn> call, Response<ResponseSignIn> response) {
                ResponseSignIn responseSignIn = response.body();
                Log.i(ApiHelper.this.getClass().getSimpleName(), "signIn => " + responseSignIn.getUser());

                if (responseSignIn.getSuccess()) {

                    listener.onResponse(responseSignIn.getSuccess(), responseSignIn.getUser(), null);

                    User temp = responseSignIn.getUser();
                    temp.setPassword(sifre);

                    localListener.signIn(temp);

                } else {
                    Log.i(this.getClass().getSimpleName(), "signIn Response Code =>" + ApiUtil.convert(String.valueOf(responseSignIn.getMsg())));
                    listener.onResponse(false, null, ApiUtil.convert(String.valueOf(responseSignIn.getMsg())));
                }

            }

            @Override
            public void onFailure(Call<ResponseSignIn> call, Throwable t) {
                Log.i(this.getClass().getSimpleName(), "signIn Failure msg : " + t.getMessage());
                listener.onFailure(t.getMessage());
            }
        });
    }

    //Add
    public void addCategory(Category category, String userId, final Api.AddCategory listener) {

        Call<ResponseAddCategory> call = RetrofitClient
                .getInstance()
                .getApi()
                .addCategory(new BodyAddCategory(userId, category));

        call.enqueue(new Callback<ResponseAddCategory>() {
            @Override
            public void onResponse(Call<ResponseAddCategory> call, Response<ResponseAddCategory> response) {
                ResponseAddCategory responseAddCategory = response.body();
                if (responseAddCategory.getSuccess()) {
                    listener.onResponse(responseAddCategory.getSuccess(), responseAddCategory.getCategory(), null);
                } else {
                    listener.onResponse(responseAddCategory.getSuccess(), null, ApiUtil.convert(String.valueOf(responseAddCategory.getMsg())));
                }
            }

            @Override
            public void onFailure(Call<ResponseAddCategory> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }

    public void addTransaction(Transaction transaction, String userId, final Api.AddTransaction listener) {
        Log.i(ApiHelper.this.getClass().getSimpleName(), "addTransaction transaction =>" + transaction.toString() + " userId => " + userId + " listener => " + listener.toString());

        Call<ResponseAddTransaction> call = RetrofitClient
                .getInstance()
                .getApi()
                .addTransaction(new BodyAddTransaction(userId, transaction));

        call.enqueue(new Callback<ResponseAddTransaction>() {
            @Override
            public void onResponse(Call<ResponseAddTransaction> call, Response<ResponseAddTransaction> response) {
                ResponseAddTransaction responseAddTransaction = response.body();

                Log.i(ApiHelper.this.getClass().getSimpleName(), "onResponse response =>" + responseAddTransaction.toString());

                if (responseAddTransaction.getSuccess()) {
                    listener.onResponse(responseAddTransaction.getSuccess(), responseAddTransaction.getTransaction(), null);
                } else {
                    listener.onResponse(responseAddTransaction.getSuccess(), null, ApiUtil.convert(String.valueOf(responseAddTransaction.getMsg())));
                }
            }

            @Override
            public void onFailure(Call<ResponseAddTransaction> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }

    //Get
    public void getTransactions(String userId, String categoryId, final Api.GetTransactions listener) {
        Call<ResponseGetTransactions> call = RetrofitClient
                .getInstance()
                .getApi()
                .getTransactions(userId, categoryId);

        call.enqueue(new Callback<ResponseGetTransactions>() {
            @Override
            public void onResponse(Call<ResponseGetTransactions> call, Response<ResponseGetTransactions> response) {
                ResponseGetTransactions responseGetTransactions = response.body();
                if (response.isSuccessful()) {
                    if (responseGetTransactions.getSuccess()) {
                        listener.onResponse(responseGetTransactions.getSuccess(), responseGetTransactions.getTransactions());
                    } else {
                        listener.onResponse(responseGetTransactions.getSuccess(), null);
                    }
                } else {
                    listener.onResponse(false, null);
                }
            }

            @Override
            public void onFailure(Call<ResponseGetTransactions> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }

    public void getCategories(String userId, final Api.GetCategories listener) {
        Log.i(this.getClass().getSimpleName(), "getCategories userId => " + userId + " listener => " + listener.toString());

        Call<ResponseGetCategories> call = RetrofitClient
                .getInstance()
                .getApi()
                .getCategories(userId);

        call.enqueue(new Callback<ResponseGetCategories>() {
            @Override
            public void onResponse(Call<ResponseGetCategories> call, Response<ResponseGetCategories> response) {
                ResponseGetCategories responseGetCategories = response.body();

                Log.i(ApiHelper.this.getClass().getSimpleName(), "getCategories response => " + response.body().toString());

                Log.i(ApiHelper.this.getClass().getSimpleName(), "getCategories success => " + responseGetCategories.getSuccess());
                if (responseGetCategories.getSuccess()) {
                    for (Category category : responseGetCategories.getCategories()) {
                        Log.i(ApiHelper.this.getClass().getSimpleName(), "getCategories categoryId => " + category.getId());
                    }
                    listener.onResponse(responseGetCategories.getSuccess(), responseGetCategories.getCategories());
                } else {
                    listener.onResponse(responseGetCategories.getSuccess(), null);
                }
            }

            @Override
            public void onFailure(Call<ResponseGetCategories> call, Throwable t) {
                Log.i(this.getClass().getSimpleName(), "getCategories onFailure.");
                listener.onFailure(t.getMessage());
            }
        });
    }

    //Change Password
    public void changePassword(final BodyChangePassword bodyChangePassword, final Api.ChangePassword listener) {
        Call<ResponseChangePassword> call = RetrofitClient
                .getInstance()
                .getApi()
                .changePassword(bodyChangePassword);

        call.enqueue(new Callback<ResponseChangePassword>() {
            @Override
            public void onResponse(Call<ResponseChangePassword> call, Response<ResponseChangePassword> response) {
                ResponseChangePassword responseChangePassword = response.body();

                if (response.isSuccessful()) {

                    if (responseChangePassword.getSuccess()) {
                        listener.onResponse(responseChangePassword.getSuccess(), "");
                        localListener.changePassword(bodyChangePassword.newPassword);
                    } else {
                        listener.onResponse(responseChangePassword.getSuccess(), ApiUtil.convert(String.valueOf(responseChangePassword.getMsg())));
                    }
                } else {
                    listener.onResponse(false, ApiUtil.CODE.BASE_ERROR);
                }

            }

            @Override
            public void onFailure(Call<ResponseChangePassword> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }

    public void forgetPassword(String email, final Api.ForgetPassword listener) {
        Log.i(this.getClass().getSimpleName(), "forgetPassword email => " + email + " listener => " + listener.toString());

        Call<ResponseForgetPassword> call = RetrofitClient
                .getInstance()
                .getApi()
                .forgetPassword(email);

        call.enqueue(new Callback<ResponseForgetPassword>() {
            @Override
            public void onResponse(Call<ResponseForgetPassword> call, Response<ResponseForgetPassword> response) {
                ResponseForgetPassword responseForgetPassword = response.body();

                if (response.isSuccessful()) {
                    listener.onResponse(responseForgetPassword.getSuccess(), responseForgetPassword.getMsg() != null ? ApiUtil.convert(String.valueOf(responseForgetPassword.getMsg())) : "New password cannot send!");
                } else {
                    listener.onResponse(false, "New password cannot send!");
                }
            }

            @Override
            public void onFailure(Call<ResponseForgetPassword> call, Throwable t) {
                Log.i(this.getClass().getSimpleName(), "forgetPassword onFailure.");
                listener.onFailure(t.getMessage());
            }
        });
    }

    public void getMoney(String userId, final Api.MoneyInfo listener) {
        Log.i(this.getClass().getSimpleName(), "getMoney userId => " + userId + " listener => " + listener.toString());

        Call<ResponseMoneyInfo> call = RetrofitClient
                .getInstance()
                .getApi()
                .getMoneyInfo(userId);

        call.enqueue(new Callback<ResponseMoneyInfo>() {
            @Override
            public void onResponse(Call<ResponseMoneyInfo> call, Response<ResponseMoneyInfo> response) {
                ResponseMoneyInfo responseMoneyInfo = response.body();

                if (responseMoneyInfo.getSuccess()) {
                    listener.onResponse(responseMoneyInfo);
                } else {
                    listener.onResponse(responseMoneyInfo);
                }
            }

            @Override
            public void onFailure(Call<ResponseMoneyInfo> call, Throwable t) {
                Log.i(this.getClass().getSimpleName(), "getMoney onFailure.");
                listener.onFailure(t.getMessage());
            }
        });
    }

    public void deleteCategory(String userId, final String categoryId, final Api.DeleteCategory listener) {
        Call<ResponseDeleteCategory> call = RetrofitClient
                .getInstance()
                .getApi()
                .deleteCategory(new BodyDeleteCategory(userId, categoryId));

        call.enqueue(new Callback<ResponseDeleteCategory>() {
            @Override
            public void onResponse(Call<ResponseDeleteCategory> call, Response<ResponseDeleteCategory> response) {
                ResponseDeleteCategory responseDeleteTransaction = response.body();
                if (response.isSuccessful()) {
                    listener.onResponse(responseDeleteTransaction.getSuccess());
                    if (responseDeleteTransaction.getSuccess()) {
                        localListener.deleteCategory(categoryId);
                    }
                } else {
                    listener.onResponse(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseDeleteCategory> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }

    public void deleteTransaction(String userId, String transactionId, final Api.DeleteTransaction listener) {
        Call<ResponseDeleteTransaction> call = RetrofitClient
                .getInstance()
                .getApi()
                .deleteTransaction(new BodyDeleteTransaction(userId, transactionId));

        call.enqueue(new Callback<ResponseDeleteTransaction>() {
            @Override
            public void onResponse(Call<ResponseDeleteTransaction> call, Response<ResponseDeleteTransaction> response) {
                ResponseDeleteTransaction responseDeleteTransaction = response.body();
                if (responseDeleteTransaction.getSuccess()) {
                    listener.onResponse(responseDeleteTransaction.getSuccess(), null);
                } else {
                    listener.onResponse(responseDeleteTransaction.getSuccess(), ApiUtil.convert(String.valueOf(responseDeleteTransaction.getMsg())));
                }
            }

            @Override
            public void onFailure(Call<ResponseDeleteTransaction> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }

    public void editTransaction(String userId, Transaction transaction, final Api.EditTransaction listener) {
        Log.i(ApiHelper.this.getClass().getSimpleName(), "editTransaction transaction =>" + transaction.toString() + " userId => " + userId + " listener => " + listener.toString());

        Call<ResponseEditTransaction> call = RetrofitClient
                .getInstance()
                .getApi()
                .editTransaction(new BodyEditTransaction(userId, transaction.getId(), transaction));

        call.enqueue(new Callback<ResponseEditTransaction>() {
            @Override
            public void onResponse(Call<ResponseEditTransaction> call, Response<ResponseEditTransaction> response) {
                ResponseEditTransaction responseEditTransaction = response.body();

                Log.i(ApiHelper.this.getClass().getSimpleName(), "onResponse response =>" + responseEditTransaction.toString());

                if (responseEditTransaction.getSuccess()) {
                    listener.onResponse(responseEditTransaction.getSuccess(), null);
                } else {
                    listener.onResponse(responseEditTransaction.getSuccess(), ApiUtil.convert(String.valueOf(responseEditTransaction.getMsg())));
                }
            }

            @Override
            public void onFailure(Call<ResponseEditTransaction> call, Throwable t) {
                listener.onFailure(t.getMessage());
            }
        });
    }
}

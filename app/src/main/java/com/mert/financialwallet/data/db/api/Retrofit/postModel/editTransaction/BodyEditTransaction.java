package com.mert.financialwallet.data.db.api.Retrofit.postModel.editTransaction;

import com.mert.financialwallet.data.model.Transaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodyEditTransaction {
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("transaction")
    @Expose
    private Transaction transaction;

    /**
     * No args constructor for use in serialization
     *
     */
    public BodyEditTransaction() {
    }

    /**
     *
     * @param transactionId
     * @param transaction
     * @param userId
     */
    public BodyEditTransaction(String userId, String transactionId, Transaction transaction) {
        super();
        this.userId = userId;
        this.transactionId = transactionId;
        this.transaction = transaction;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public String toString() {
        return "BodyEditTransaction{" +
                "userId='" + userId + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", transaction=" + transaction +
                '}';
    }
}

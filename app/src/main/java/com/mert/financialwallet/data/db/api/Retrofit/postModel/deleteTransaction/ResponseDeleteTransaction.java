package com.mert.financialwallet.data.db.api.Retrofit.postModel.deleteTransaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseDeleteTransaction {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("msg")
    @Expose
    private Integer msg;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseDeleteTransaction() {
    }

    /**
     *
     * @param msg
     * @param success
     */
    public ResponseDeleteTransaction(Boolean success, Integer msg) {
        super();
        this.success = success;
        this.msg = msg;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getMsg() {
        return msg;
    }

    public void setMsg(Integer msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ResponseDeleteTransaction{" +
                "success=" + success +
                ", msg=" + msg +
                '}';
    }
}

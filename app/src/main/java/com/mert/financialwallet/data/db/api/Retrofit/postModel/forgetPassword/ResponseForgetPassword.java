package com.mert.financialwallet.data.db.api.Retrofit.postModel.forgetPassword;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseForgetPassword {

    @SerializedName("success")
    @Expose
    public Boolean success;

    @Nullable
    @SerializedName("msg")
    @Expose
    public Integer msg;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseForgetPassword() {
    }

    /**
     *
     * @param msg
     * @param success
     */
    public ResponseForgetPassword(Boolean success, Integer msg) {
        super();
        this.success = success;
        this.msg = msg;
    }

    public Boolean getSuccess() {
        return success;
    }

    @Nullable
    public Integer getMsg() {
        return msg;
    }

}

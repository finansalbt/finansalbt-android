package com.mert.financialwallet.data.db.api;

import com.mert.financialwallet.data.db.api.Retrofit.postModel.getInfoMoney.ResponseMoneyInfo;
import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.mert.financialwallet.data.model.User;

import java.util.ArrayList;
import java.util.List;

public interface Api {

    interface AddTransaction extends Api {
        void onResponse(boolean isTrue, Transaction transaction, String responseText);

        void onFailure(String responseText);
    }

    interface AddCategory extends Api {
        void onResponse(boolean isTrue, Category category, String responseText);

        void onFailure(String responseText);
    }


    interface GetTransactions extends Api {
        void onResponse(boolean isTrue, List<Transaction> transactions);

        void onFailure(String responseText);
    }

    interface GetCategories extends Api {
        void onResponse(boolean isTrue, List<Category> categories);

        void onFailure(String responseText);
    }

    interface ForgetPassword extends Api {
        void onResponse(boolean isTrue, String responseText);

        void onFailure(String responseText);
    }

    interface ChangePassword extends Api {
        void onResponse(boolean isTrue, String responseText);

        void onFailure(String responseText);
    }

    interface SignUp extends Api {
        void onResponse(boolean isTrue, String responseText);

        void onFailure(String responseText);
    }

    interface SignIn extends Api {
        void onResponse(boolean isTrue, User user, String responseText);

        void onFailure(String responseText);
    }

    interface SignOut extends Api {
        void signOut(boolean state);
    }

    interface Local {
        void signIn(User user);

        void changePassword(String password);

        void deleteCategory(String categoryId);
    }

    interface Init extends Api {
        void signIn(String tokenResponse);
    }

    interface MoneyInfo extends Api {
        void onResponse(ResponseMoneyInfo responseMoneyInfo);

        void onFailure(String responseText);
    }

    interface DeleteCategory extends Api {
        void onResponse(boolean isTrue);

        void onFailure(String responseText);
    }

    interface DeleteTransaction extends Api {
        void onResponse(boolean isTrue, String responseText);

        void onFailure(String responseText);
    }

    interface EditTransaction extends Api {
        void onResponse(boolean isTrue, String responseText);

        void onFailure(String responseText);
    }
}

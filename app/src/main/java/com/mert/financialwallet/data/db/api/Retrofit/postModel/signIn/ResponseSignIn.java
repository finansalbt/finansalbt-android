package com.mert.financialwallet.data.db.api.Retrofit.postModel.signIn;

import android.support.annotation.Nullable;

import com.mert.financialwallet.data.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseSignIn {

    @SerializedName("success")
    private Boolean success;

    @SerializedName("user")
    @Nullable
    private User user;

    @SerializedName("msg")
    @Nullable
    private String msg;


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}

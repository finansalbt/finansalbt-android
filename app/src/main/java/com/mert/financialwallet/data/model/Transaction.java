package com.mert.financialwallet.data.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {


    @SerializedName("_id")
    @Expose
    @Nullable
    private String id;

    @SerializedName("type")
    @Expose
    private Boolean type;

    @SerializedName("amount")
    @Expose
    private double amount;

    @SerializedName("currency_unit")
    @Expose
    private String currencyUnit;

    @SerializedName("category_id")
    @Nullable
    @Expose
    private String categoryId;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("created_at")
    @Expose
    @Nullable
    private long createdAt;

    /**
     * No args constructor for use in serialization
     *
     */
    public Transaction() {
    }

    public Transaction(Boolean type, double amount, String currencyUnit, String categoryId, String description) {
        this.type = type;
        this.amount = amount;
        this.currencyUnit = currencyUnit;
        this.categoryId = categoryId;
        this.description = description;
    }

    public Transaction(String id,Boolean type, double amount, String currencyUnit, String categoryId, String description) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.currencyUnit = currencyUnit;
        this.categoryId = categoryId;
        this.description = description;
    }

    /**
     *
     * @param amount
     * @param id
     * @param currencyUnit
     * @param createdAt
     * @param description
     * @param categoryId
     * @param type
     */
    public Transaction(String id, Boolean type, double amount, String currencyUnit, String categoryId, String description, long createdAt) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.currencyUnit = currencyUnit;
        this.categoryId = categoryId;
        this.description = description;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", amount=" + amount +
                ", currencyUnit='" + currencyUnit + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", description='" + description + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}

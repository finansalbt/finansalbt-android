package com.mert.financialwallet.data.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {

    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;

    @SerializedName("transactions")
    @Expose
    private List<Transaction> transactions = null;

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("fullname")
    @Expose
    private String fullname;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("password")
    @Expose
    @Nullable
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEMail() {
        return email;
    }

    public void setEMail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    //Category
    public List<Category> getCategories() {
        if (categories == null) {
            categories = new ArrayList<>();
        }
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public String getCategoryNamebyId(String id) {
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getId().equals(id)) {
                return getCategories().get(i).getName();
            }
        }
        return "";
    }

    public String getCategorytIdbyName(String name) {

        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getName().toUpperCase().equals(name.toUpperCase())) {
                return getCategories().get(i).getId();
            }
        }
        return "";
    }


    //PostTransaction
    public List<Transaction> getTransactions() {
        if (transactions == null) {
            transactions = new ArrayList<>();
        }
        return transactions;
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }

    public ArrayList<Transaction> getTransactionbyCategories(String categoryId) {
        ArrayList<Transaction> transactionArrayList = new ArrayList<>();

        for (int i = 0; i < getTransactions().size(); i++) {
            if (getTransactions().get(i).getCategoryId() == categoryId) {
                transactionArrayList.add(getTransactions().get(i));
            }
        }

        return transactionArrayList;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + getId() + '\'' +
                ", fullname='" + getFullname() + '\'' +
                ", eMail='" + getEMail() + '\'' +
                ", username='" + getUsername() + '\'' +
                ", categories=" + getCategories() +
                ", transactions=" + getTransactions() +
                ", password='" + getPassword() + '\'' +
                '}';
    }
}

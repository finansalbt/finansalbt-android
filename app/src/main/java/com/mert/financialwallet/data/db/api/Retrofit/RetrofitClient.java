package com.mert.financialwallet.data.db.api.Retrofit;

import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    //public static final String BASE_URL = "http://10.0.2.2:80/";
    public static final String BASE_URL = "https://finansalbtapi.azurewebsites.net/";

    private static RetrofitClient instance;

    private static retrofit2.Retrofit retrofit = null;

    public RetrofitClient() {
        retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (instance == null) {
            instance = new RetrofitClient();
        }

        return instance;
    }

    public Retrofit getApi() {
        return retrofit.create(Retrofit.class);
    }
}

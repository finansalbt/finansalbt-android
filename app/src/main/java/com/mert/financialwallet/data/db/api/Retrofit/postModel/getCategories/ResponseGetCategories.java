package com.mert.financialwallet.data.db.api.Retrofit.postModel.getCategories;

import android.support.annotation.Nullable;

import com.mert.financialwallet.data.model.Category;
import com.mert.financialwallet.data.model.Transaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetCategories {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("categories")
    @Nullable
    @Expose
    private List<Category> categories;

    /**
     * No args constructor for use in serialization
     */
    public ResponseGetCategories() {
    }

    /**
     * @param categories
     * @param success
     */
    public ResponseGetCategories(boolean success, List<Category> categories) {
        super();
        this.success = success;
        this.categories = categories;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "ResponseGetCategories{" +
                "success=" + success +
                ", categories=" + categories +
                '}';
    }
}

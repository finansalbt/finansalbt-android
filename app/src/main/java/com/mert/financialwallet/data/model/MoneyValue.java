package com.mert.financialwallet.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoneyValue {

    @SerializedName("TRY_income")
    @Expose
    private double tRYIncome;
    @SerializedName("TRY_expense")
    @Expose
    private double tRYExpense;
    @SerializedName("USD_income")
    @Expose
    private double uSDIncome;
    @SerializedName("USD_expense")
    @Expose
    private double uSDExpense;
    @SerializedName("EUR_income")
    @Expose
    private double eURIncome;
    @SerializedName("EUR_expense")
    @Expose
    private double eURExpense;
    @SerializedName("TRY")
    @Expose
    private double tRY;
    @SerializedName("USD")
    @Expose
    private double uSD;
    @SerializedName("EUR")
    @Expose
    private double eUR;

    /**
     * No args constructor for use in serialization
     *
     */
    public MoneyValue() {
    }

    /**
     *
     * @param uSD
     * @param tRYIncome
     * @param tRY
     * @param uSDIncome
     * @param eURExpense
     * @param eUR
     * @param uSDExpense
     * @param eURIncome
     * @param tRYExpense
     */
    public MoneyValue(double tRYIncome, double tRYExpense, double uSDIncome, double uSDExpense, double eURIncome, double eURExpense, double tRY, double uSD, double eUR) {
        super();
        this.tRYIncome = tRYIncome;
        this.tRYExpense = tRYExpense;
        this.uSDIncome = uSDIncome;
        this.uSDExpense = uSDExpense;
        this.eURIncome = eURIncome;
        this.eURExpense = eURExpense;
        this.tRY = tRY;
        this.uSD = uSD;
        this.eUR = eUR;
    }

    public double getTRYIncome() {
        return tRYIncome;
    }

    public void setTRYIncome(double tRYIncome) {
        this.tRYIncome = tRYIncome;
    }

    public double getTRYExpense() {
        return tRYExpense;
    }

    public void setTRYExpense(double tRYExpense) {
        this.tRYExpense = tRYExpense;
    }

    public double getUSDIncome() {
        return uSDIncome;
    }

    public void setUSDIncome(double uSDIncome) {
        this.uSDIncome = uSDIncome;
    }

    public double getUSDExpense() {
        return uSDExpense;
    }

    public void setUSDExpense(double uSDExpense) {
        this.uSDExpense = uSDExpense;
    }

    public double getEURIncome() {
        return eURIncome;
    }

    public void setEURIncome(double eURIncome) {
        this.eURIncome = eURIncome;
    }

    public double getEURExpense() {
        return eURExpense;
    }

    public void setEURExpense(double eURExpense) {
        this.eURExpense = eURExpense;
    }

    public double getTRY() {
        return tRY;
    }

    public void setTRY(double tRY) {
        this.tRY = tRY;
    }

    public double getUSD() {
        return uSD;
    }

    public void setUSD(double uSD) {
        this.uSD = uSD;
    }

    public double getEUR() {
        return eUR;
    }

    public void setEUR(double eUR) {
        this.eUR = eUR;
    }

    @Override
    public String toString() {
        return "MoneyValue{" +
                "tRYIncome=" + tRYIncome +
                ", tRYExpense=" + tRYExpense +
                ", uSDIncome=" + uSDIncome +
                ", uSDExpense=" + uSDExpense +
                ", eURIncome=" + eURIncome +
                ", eURExpense=" + eURExpense +
                ", tRY=" + tRY +
                ", uSD=" + uSD +
                ", eUR=" + eUR +
                '}';
    }
}

package com.mert.financialwallet.data.db.api.Retrofit.postModel.changePassword;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseChangePassword {


    @SerializedName("success")
    @Expose
    public boolean success;

    @SerializedName("msg")
    @Expose
    @Nullable
    public Integer msg;

    /**
     * No args constructor for use in serialization
     *
     */
    public ResponseChangePassword() {
    }

    /**
     *
     * @param msg
     * @param success
     */
    public ResponseChangePassword(Boolean success, Integer msg) {
        super();
        this.success = success;
        this.msg = msg;
    }

    public Boolean getSuccess() {
        return success;
    }

    public Integer getMsg() {
        return msg;
    }
}

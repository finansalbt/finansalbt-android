package com.mert.financialwallet.data.db.api.Retrofit.postModel.signUp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BodySignUp {

    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;

    /**
     * No args constructor for use in serialization
     *
     */
    public BodySignUp() {
    }

    /**
     *
     * @param username
     * @param email
     * @param password
     * @param fullname
     */
    public BodySignUp(String fullname, String email, String username, String password) {
        super();
        this.fullname = fullname;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
